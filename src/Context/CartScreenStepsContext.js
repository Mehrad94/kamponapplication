import createDtateContext from "./createDataContext";
import { getItemsInCart } from "../API/getItemsInCart";
import { DeleteFromCart } from "../API";
import { userCartAction } from "../API/userCartAction";
const STEP_CHANGE = "step_change";
const SET_CART_ITEMS = "set_cart_items";
const SET_TOTAL = "set_total";
const BUTTON_ACTIVE = "button_active";
const ON_DEACREASE_ID = "on_deacrease_id";
const ON_INCREASE_ID = "on_increase_id";
const SET_COUNT = "set_count";
const INCREASE = "increase";
const DEACREASE = "decrease";
const DELETE_CART = "delete_cart";
const ADD_ERROR = "add_error";
const DELETE_ERROR = "delete_error";
const CartScreenStepsContext = (state, action) => {
  switch (action.type) {
    case STEP_CHANGE:
      return { ...state, step: action.payload };
    case SET_CART_ITEMS:
      return { ...state, cartItems: action.payload };
    case ON_INCREASE_ID:
      return { ...state, increaseId: action.payload };
    case ON_DEACREASE_ID:
      return { ...state, deacreaseId: action.payload };
    case BUTTON_ACTIVE:
      return { ...state, buttonActive: action.payload };
    case SET_TOTAL:
      return { ...state, total: action.payload };
    case SET_COUNT:
      return { ...state, count: action.payload };

    case DELETE_CART:
    //  return {...state}
    case ADD_ERROR:
      return { ...state, error: action.payload };
    case DELETE_ERROR:
      return { ...state, error: "" };
    default:
      return state;
  }
};
const onStepChange = (dispatch) => (step) => {
  dispatch({ type: STEP_CHANGE, payload: step });
};
const setCartItems = (dispatch) => async (callBack) => {
  const resCart = await getItemsInCart();
  console.log({ resCart });
  if (typeof resCart === "object") {
    dispatch({ type: SET_CART_ITEMS, payload: resCart });
    if (callBack) {
      callBack();
    }
  } else {
    console.log("error in cart fetch");
  }
};
const setTotal = (dispatch) => (total) => {
  dispatch({ type: SET_TOTAL, payload: total });
};
const setButtonActive = (dispatch) => (bool) => {
  dispatch({ type: BUTTON_ACTIVE, payload: bool });
};
const addToCart = (dispatch) => async (id, count, callBack) => {
  dispatch({ type: DELETE_ERROR });
  const resAdd = await userCartAction(id, count);
  if (resAdd.CODE === 2017) {
    console.log({ resAdd });
    if (callBack) {
      callBack();
    }
  } else {
    dispatch({ type: ADD_ERROR, payload: "SMT_WRONG_IN_ADD_CART" });
  }
};
const deletCart = (dispatch) => async (id, callBack) => {
  dispatch({ type: DELETE_ERROR });
  const resDelete = await DeleteFromCart(id);
  if (resDelete.CODE === 2018) {
    console.log({ resDelete });
    dispatch({ type: "change_items_in_item_count", payload: id });
    if (callBack) {
      callBack();
    }
  } else {
    dispatch({ type: ADD_ERROR, payload: "SMT_WRONG_IN_DELET_CART" });
  }
};
const chnageCartItemsAction = (dispatch) => () => {
  dispatch({ type: "change_items_in_item_count" });
};

export const { Provider, Context } = createDtateContext(
  CartScreenStepsContext,
  {
    onStepChange,
    setCartItems,
    setTotal,
    setButtonActive,
    deletCart,
    addToCart,
    chnageCartItemsAction
  },
  {
    step: 0,
    cartItems: [],
    total: 0,
    buttonActive: false,
    deacreaseId: null,
    increaseId: null,
    count: 1,
    error: ""
  }
);
