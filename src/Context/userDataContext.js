import createDtateContext from "./createDataContext";
import { getMemberInfo } from "../API/getMemberInfo";
const USER_INFO = "user_info";
const UserDataContext = (state = inisialSatate, action) => {
  switch (action.type) {
    case USER_INFO:
      return { ...state, userInfo: action.payload };
    default:
      return state;
  }
};
const inisialSatate = {
  userInfo: null
};
const getMemberInformation = (dispatch) => async () => {
  const resUser = await getMemberInfo();
  console.log({ resUser });
  if (typeof resUser === "object") {
    dispatch({ type: USER_INFO, payload: resUser });
  }
};
export const { Provider, Context } = createDtateContext(
  UserDataContext,
  { getMemberInformation },
  inisialSatate
);
