import createDtateContext from "./createDataContext";
import { getAllDiscounts } from "../API/getAllDiscounts";
import { GetClubData } from "../API/GetClubData";
import { getAllClubsData } from "../API/getAllClubsData";
import { getCategories } from "../API/getCategories";
import { getCategoriesData } from "../API/getCategoriesData";
import { getSplashData } from "../API/getSplashData";
import { getScenarioSpinner } from "../API/getScenarioSpinner";
const FETCH_ALL_DISCOUNTS = "fetch_all_discounts";
const GET_CLUBS_DATA = "get_all_clubs_data";
const GET_CATEGORIES = "get_categories";
const GET_CAT_DATA = "get-cat-data";
const SET_CATEGORIES = "set_catrgoties";
const SET_USER = "set_user";
const SET_HOMESCREEN = "set_home_screen";
const CITY_FILTER = "city_filter";
const TYPE_FILTER = "type_filter";
const SET_GIFT_ARRAY = "set_gift_array";
const SET_SPIN_TIME = "spin";
const DataContext = (state, action) => {
  switch (action.type) {
    case "shimmer_swicher":
      return { ...state, isShimmerVisible: action.payload };
    case "home_data":
      return { ...state, homeData: action.payload };
    case FETCH_ALL_DISCOUNTS:
      return { ...state, allDiscounts: action.payload };
    case GET_CLUBS_DATA:
      return { ...state, allClubsData: action.payload };
    case GET_CATEGORIES:
      return { ...state, categories: action.payload };
    case GET_CAT_DATA:
      return { ...state, catData: action.payload };
    case SET_CATEGORIES:
      return { ...state, categoiresState: action.payload };
    case SET_USER:
      return { ...state, userState: action.payload };
    case SET_HOMESCREEN:
      return { ...state, homeScreenDataState: action.payload };
    case CITY_FILTER:
      return { ...state, cityFilter: action.payload };
    case TYPE_FILTER:
      return { ...state, typeFilter: action.payload };
    case SET_GIFT_ARRAY:
      console.log({ aaaa: action.payload });
      return { ...state, giftArray: action.payload };
    case SET_SPIN_TIME:
      return { ...state, timeSpin: action.payload };

    case "gift_spin":
      return { ...state, gift: action.payload };
    case "max_spin":
      return { ...state, maxSpin: action.payload };
    default:
      return state;
  }
};

const addDataToState = (dispatch) => (data) => {
  dispatch({ type: "add_data", payload: data });
};
const shimmerSwicher = (dispatch) => (data) => {
  console.log("context");

  setTimeout(() => {
    dispatch({ type: "shimmer_swicher", payload: true });
  }, 5000);
};
const getData = (dispatch) => (data) => {
  dispatch({ type: "home_data", payload: data });
};
const fetchAlldiscounts = (dispatch) => async (page) => {
  const res = await getAllDiscounts();

  dispatch({ type: FETCH_ALL_DISCOUNTS, payload: res });
};
const fetchClubsData = (dispatch) => async (page) => {
  const res = await getAllClubsData();
  console.log({ res });

  dispatch({ type: GET_CLUBS_DATA, payload: res });
};
const fetchCaregories = (dispatch) => async (data) => {
  const res = await getCategories();
  dispatch({ type: GET_CATEGORIES, payload: res });
};
const fetchCategoriesData = (dispatch) => async (disId, callBack) => {
  const res = await getCategoriesData(disId);
  dispatch({ type: GET_CAT_DATA, payload: res });
};
const fetchSplashData = (dispatch) => async (callBack) => {
  const resSplashData = await getSplashData();
  console.log({ resSplashData });

  if (typeof resSplashData === "object") {
    dispatch({ type: SET_CATEGORIES, payload: resSplashData.categories });
    dispatch({ type: SET_USER, payload: resSplashData.user });
    dispatch({ type: SET_HOMESCREEN, payload: resSplashData.homeScreen });
  }
  if (callBack) {
    callBack();
  }
};
const setCityFilter = (dispatch) => (item) => {
  dispatch({ type: CITY_FILTER, payload: item });
};
const setTypeFilter = (dispatch) => (item) => {
  dispatch({ type: TYPE_FILTER, payload: item });
};
const spinnerActions = (dispatch) => async () => {
  function shuffle(array) {
    var currentIndex = array.length,
      temporaryValue,
      randomIndex;
    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }
  const resScenario = await getScenarioSpinner();
  console.log({ resScenario });
  const gift = resScenario.scenario.gifts;
  const empty = resScenario.scenario.empty;
  let giftArray = [...gift, ...empty];
  shuffle(giftArray);
  console.log({ giftArray });
  if (typeof giftArray === "object") {
    console.log("mori");

    dispatch({ type: SET_SPIN_TIME, payload: resScenario.canSpin });
    dispatch({ type: SET_GIFT_ARRAY, payload: giftArray });
    dispatch({ type: "gift_spin", payload: resScenario.gift });
    dispatch({ type: "max_spin", payload: resScenario.nextSpin });
    console.log("dori");
  }
};
export const { Provider, Context } = createDtateContext(
  DataContext,
  {
    shimmerSwicher,
    getData,
    fetchAlldiscounts,
    fetchCategoriesData,
    fetchClubsData,
    fetchCaregories,
    fetchSplashData,
    setTypeFilter,
    setCityFilter,
    spinnerActions,
  },
  {
    isShimmerVisible: false,
    homeData: null,
    categories: [],
    allDiscounts: null,
    allClubsData: null,
    categories: null,
    catData: null,
    categoiresState: [],
    userState: null,
    homeScreenDataState: null,
    cityFilter: "",
    typeFilter: "",
    giftArray: [],
    timeSpin: null,
    maxSpin: null,
    gift: null,
  }
);
