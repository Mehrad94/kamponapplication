import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import Sound from "react-native-sound";
import Music from "../../Assets/spin.mp3";
import { Flex, centerAll } from "../../Values/Theme";
const SoundCoponent = () => {
  let song = null;
  useEffect(() => {
    song = new Sound("spin_main.mp3", Sound.MAIN_BUNDLE, (error) => {
      console.log({ error });

      if (error) {
        console.log("failed to load the sound", error);
        return;
      }
    });
  });
  const [play, setPlay] = useState(false);
  const spinSound = () => {};
  const onPressSong = () => {
    song.play((sucsses) => {
      if (sucsses) {
        console.log("ok");
      } else {
        console.log("nok");
      }
    });
  };
  return (
    <View style={[Flex, centerAll]}>
      <TouchableOpacity
        onPress={onPressSong}
        style={{ width: 200, height: 70, backgroundColor: "red" }}
      >
        <Text> Screen</Text>
      </TouchableOpacity>
    </View>
  );
};
export default SoundCoponent;
