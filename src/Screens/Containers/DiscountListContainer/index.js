import React, { useContext, useEffect, useMemo } from "react";
import { Image, View, Text, StyleSheet } from "react-native";
import { Context as DataContext } from "../../../Context/DataContext";
import { centerAll } from "../../../Values/Theme";
import DisAndClubsContainer from "../../../Reusable/DisAndClubsContainer";
import { MOST_VIEW, MOST_NEW, TODAY_OFFER, MOST_DISCOUNT_HOME } from "../../../Common/Strings";
import FastImage from "react-native-fast-image";
const DiscountListContainer = (props) => {
  const context = useContext(DataContext);
  const { homeScreenDataState, typeFilter, cityFilter } = context.state;
  console.log(typeFilter, cityFilter);
  console.log({ MMM: context.state });

  let MostView = null;
  let firstBanner = "";
  let Newest = null;
  let SecondBanner = null;
  let TodayOffer = null;
  let ThirdBanner = null;
  let MostDiscount = null;

  if (homeScreenDataState) {
    const {
      mostView,
      banner,
      newest,
      secondBanner,
      todayOffer,
      thirdBanner,
      mostDiscount,
    } = homeScreenDataState;
    MostView = mostView;
    firstBanner = banner ? banner.image : "";
    Newest = newest;
    SecondBanner = secondBanner ? secondBanner.image : "";
    TodayOffer = todayOffer;
    ThirdBanner = thirdBanner ? thirdBanner.image : "";
    MostDiscount = mostDiscount ? mostDiscount : null;
  }

  const mostViewItems = () => {
    return <DisAndClubsContainer filter={typeFilter} data={MostView} title={MOST_VIEW} />;
  };
  const theNewestItem = () => {
    return <DisAndClubsContainer filter={typeFilter} data={Newest} title={MOST_NEW} />;
  };
  const todatsOfferItems = () => {
    return <DisAndClubsContainer filter={typeFilter} data={TodayOffer} title={TODAY_OFFER} />;
  };
  const mostDiscountItems = () => {
    return (
      <DisAndClubsContainer
        filter={typeFilter}
        data={MostDiscount}
        most={true}
        title={MOST_DISCOUNT_HOME}
      />
    );
  };
  const firstBannerImage = () => {
    if (firstBanner) {
      return <FastImage style={{ width: "100%", height: 150 }} source={{ uri: firstBanner }} />;
    } else {
      return null;
    }
  };
  const secoundBannerImage = () => {
    if (SecondBanner) {
      return <FastImage style={{ width: "100%", height: 150 }} source={{ uri: SecondBanner }} />;
    } else {
      return null;
    }
  };
  const thirdBannerImage = () => {
    if (ThirdBanner) {
      return <FastImage style={{ width: "100%", height: 150 }} source={{ uri: ThirdBanner }} />;
    } else {
      return null;
    }
  };
  return (
    <View style={Styles.container}>
      {mostViewItems()}
      {firstBannerImage()}
      {theNewestItem()}
      {secoundBannerImage()}
      {todatsOfferItems()}
      {thirdBannerImage()}
      {mostDiscountItems()}
    </View>
  );
};
export default DiscountListContainer;

const Styles = StyleSheet.create({
  container: { flex: 1, ...centerAll, marginTop: 25 },
});
