import { StyleSheet } from "react-native";

export default styles = StyleSheet.create({
  Container: { marginTop: 16 },
  DiscountListTitle: {
    textAlign: "left",
    marginLeft: 25
  },
  DiscountListTitleParent: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  DiscountMore: {
    marginLeft: 8,
    fontSize: 14,
    fontFamily: "iran_sans",
    color: "#00BCD4"
  },
  MoreHolder: {
    flexDirection: "row",
    justifyContent: "center",
    marginHorizontal: 16,
    alignItems: "center"
  }
});
