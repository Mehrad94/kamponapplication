import React, { useEffect, useState, useContext } from "react";
import CartItemList from "../../Lists/CartItemList";
import { Text, View } from "react-native";

import styles from "./styles";

import { Flex, iranSans, h4 } from "../../../Values/Theme";

import { Context as StepContext } from "../../../Context/CartScreenStepsContext";
const CartItemListContainer = () => {
  //======================== CONTEXT ====================
  const context = useContext(StepContext);
  const { setTotal } = context;
  const { cartItems, total } = context.state;

  useEffect(() => {
    totalCount();
  }, [cartItems]);

  const totalCount = () => {
    let totalCountValue = 0;
    if (cartItems.length > 0) {
      cartItems.map((count) => {
        console.log({ count });

        totalCountValue = count.discount.newPrice * count.count + totalCountValue;
      });
      setTotal(totalCountValue);
    } else {
      setTotal(totalCountValue);
    }
  };
  newPrice = total;
  return (
    <View style={[Flex]}>
      <View style={styles.cartContainer}>
        <Text style={[iranSans, h4]}>جمع کل :</Text>
        <Text style={[iranSans, h4]}>{total} تومان</Text>
      </View>
      <CartItemList items={cartItems} />
    </View>
  );
};
export default CartItemListContainer;
