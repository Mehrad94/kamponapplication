import React, { useState, useContext } from "react";
import { View } from "react-native";
import { Flex, mV8 } from "../../../Values/Theme";
import TabContent from "./TabContent";
import Tabs from "./Tabs";
import { Context as DataContext } from "../../../Context/DataContext";
const CatDataTab = () => {
  const context = useContext(DataContext);
  const { state } = context;
  const [data, setData] = useState(state.catData.discounts);

  const _onTabClick = (isActive) => {
    {
      isActive ? setData(state.catData.clubs) : setData(state.catData.discounts);
    }
  };

  return (
    <View style={[Flex, mV8]}>
      <Tabs onTabClick={_onTabClick} />
      <TabContent data={data} />
    </View>
  );
};

export default CatDataTab;
