import React, { useEffect, useState, useContext } from "react";
import { BackHandler, View, Text, FlatList } from "react-native";
import InfinityScrollCategoryContainer from "../Infinities/InfinityCategory";
import { GetSubCats } from "../../API/GetSubCat";
import SubCategoryView from "../SubCategory";
import { Context as DataContext } from "../../Context/DataContext";
import { Indicator } from "../../Components/Indicator";
import BackgroundCursor from "../../Components/BackgroundCursor/BackgroundCursor";
import CatDataTab from "./CatDataTab";
const CategoryScreen = ({ navigation }) => {
  //===========================CONTEXT =========================
  const context = useContext(DataContext);
  const { state } = context;
  //===========================CONTEXT =========================
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (state.catData) {
      setLoading(false);
    }
  }, [state]);
  if (loading) {
    return <Indicator />;
  }
  return (
    <View style={{ flex: 1, backgroundColor: "#EEEEEE" }}>
      <BackgroundCursor images={state.catData.slides} />
      <CatDataTab />
    </View>
  );
};

export default CategoryScreen;
{
  /* <View style={{ flex: 0.3 }}>
<SliderCategoryContainer selectedCat={param} navigation={navigation} />
</View>
{loding ? null : <View style={[centerAll]}>{renderItem()}</View>}
<View style={{ margin: 4, flex: 0.7 }}>
<InfinityScrollCategoryContainer
  // subCat={selectedCategory}
  selectedCat={param}
  navigation={navigation}
/>
</View> */
}
