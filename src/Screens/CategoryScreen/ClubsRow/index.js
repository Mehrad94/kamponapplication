import React, { useEffect } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import styles from "./styles";
import { bgWhite } from "../../../Values/Theme";
import { Rating } from "react-native-ratings";
import FastImage from "react-native-fast-image";
import Icon from "react-native-vector-icons/FontAwesome5";
import { navigate } from "../../../../navigationRef";

const ClubsRow = (props) => {
  const onPressItem = () => {
    navigate("ClubsScreen", { clubParams: props.item });
  };
  return (
    <TouchableOpacity onPress={onPressItem} activeOpacity={0.8}>
      <View style={styles.row}>
        <View style={styles.discountContainer}>
          <View style={[bgWhite]}>
            <FastImage
              style={styles.discountImage}
              source={{ uri: props.item.owner.thumbnail }}
              resizeMode={FastImage.resizeMode.cover}
            />
            <View style={styles.Content}>
              <View style={styles.RightContent}>
                <Text style={styles.Text}>مرکز خرید اسکار</Text>
                <Text style={styles.Text}>خرید اسکار</Text>
              </View>
              <View style={styles.Icons}>
                <View style={styles.RatingHolder}>
                  <Rating
                    startingValue={2}
                    type="star"
                    imageSize={20}
                    ratingCount={5}
                    defaultRating={1}
                    readonly={true}
                    //   onFinishRating={this.ratingCompleted}
                    style={styles.Stars}
                  />
                </View>

                <View style={styles.Icon}>
                  <Icon name="credit-card" size={20} color="#d8d8d8" style={{ marginRight: 12 }} />
                  <Icon name="mobile-alt" size={20} color="#d8d8d8" style={{ marginRight: 12 }} />
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};
export default ClubsRow;
