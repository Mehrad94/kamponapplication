import React, { useEffect, useState } from "react";
import { View, Text, ScrollView, TouchableOpacity } from "react-native";
import BackgroundCursor from "../../Components/BackgroundCursor/BackgroundCursor";
import Styles from "./style";
import MainContent from "./MainContent";
import AboutContent from "./AboutContent";
import { Indicator } from "../../Components/Indicator";
import DiscountsScreen from "../DiscountsScreen";
const ClubsScreen = ({ navigation }) => {
  const [loading, setLoading] = useState(true);
  const [disCountsData, setDisCountsData] = useState(null);
  const [clubsData, setClubsData] = useState(null);
  //=========================   DISCOUNTS_DATA ====================
  let discountsSliderData = [];
  let discountsContentData = null;
  let discontsAbuteData = null;

  //==========================  CLUBS_DATA   ========================
  let clubsSliderData = [];
  let clubsContentData = [];
  let clubsAbuotData = [];

  //================================================================
  useEffect(() => {
    const disParam = navigation.getParam("disVal");
    const clubParam = navigation.getParam("clubParams");
    console.log({ disParam });
    console.log({ clubParam });
    if (disParam) {
      setDisCountsData(disParam);
    }
    if (clubParam) {
      setClubsData(clubParam);
    }
    if (disCountsData || clubsData) {
      setLoading(false);
    }
  });
  //==========================  PREAPERING_DISCOUNTS_DATA ==========================
  if (disCountsData && disCountsData.title) {
    discountsSliderData = disCountsData.slides;
  }
  //==========================  PREAPERING_CLUBS_DATA ==============================

  if (clubsData) {
    const { comments, aboutClub } = clubsData;
    clubsSliderData = clubsData.slides;
    clubsContentData = clubsData;
    clubsAbuotData = { comments, aboutClub };
  }
  console.log(clubsData, disCountsData);
  if (loading) {
    return <Indicator />;
  }
  if (disCountsData) {
    console.log(!!disCountsData);

    return <DiscountsScreen data={disCountsData} />;
  }
  return (
    <View style={Styles.Container}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        renderToHardwareTextureAndroid
        scrollEventThrottle={1}
        alwaysBounceVertical
      >
        <BackgroundCursor images={clubsSliderData} />
        <MainContent data={clubsContentData} />
        <AboutContent title={clubsContentData.title} data={clubsAbuotData} />
        <TouchableOpacity style={Styles.Footer}>
          <Text style={Styles.Text}>عضویت در باشگاه</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};
export default ClubsScreen;
