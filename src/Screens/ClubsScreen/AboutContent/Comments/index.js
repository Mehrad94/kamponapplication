import React, { useEffect } from "react";
import { View, Text, FlatList, Image } from "react-native";
const Data = [
  " برنامه‌نویسی و شگفتی‌های دنیای دیجیتالرنامه‌نویسی و شگفتی‌های دنیای دیجیتال",
  "جاوااسکریپت شیوا",
  "و شگفتی‌های دنیای دیجیتالرنامه‌نویسی "
];
import styles from "./styles";
import profileImage from "../../../../Assets/Images/heart.png";
const Comments = () => {
  const renderItem = ({ item }) => {
    return (
      <View style={styles.containerII}>
        <View style={styles.rowHolder}>
          <View style={styles.commentsHolder}>
            <Text style={styles.Text}>{item}</Text>
          </View>
          <View style={styles.profileImageHolder}>
            <Image style={styles.profileImage} source={profileImage} />
          </View>
        </View>
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <FlatList data={Data} renderItem={renderItem} />
    </View>
  );
};
export default Comments;
