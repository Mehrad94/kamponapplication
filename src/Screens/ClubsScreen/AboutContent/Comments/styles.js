import { StyleSheet } from "react-native";
import { MAIN_COLOR } from "../../../../Values/Colors";
import { iranSans } from "../../../../Values/Theme";
const Styles = StyleSheet.create({
  container: {
    flex: 1
  },
  containerII: {
    flex: 1
  },
  rowHolder: {
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "flex-end",
    marginTop: 8
  },
  profileImage: {
    width: 40,
    height: 40,
    borderRadius: 100
  },
  profileImageHolder: {
    backgroundColor: "black",
    width: 48,
    height: 48,
    borderTopLeftRadius: 100,
    borderTopRightRadius: 100,
    borderBottomRightRadius: 100,
    alignItems: "center",
    justifyContent: "center"
  },
  commentsHolder: {
    padding: 8,
    borderTopLeftRadius: 50,
    borderBottomLeftRadius: 50,
    borderTopRightRadius: 70,
    backgroundColor: MAIN_COLOR,
    justifyContent: "center",
    alignItems: "flex-end"
  },
  Text: {
    ...iranSans,
    maxWidth: 280,
    marginHorizontal: 8
  }
});
export default Styles;
