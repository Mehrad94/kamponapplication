import { Accordion } from "native-base";
import React from "react";
import { Text, View } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import { MAIN_COLOR } from "../../../Values/Colors";
import Styles from "./styles";
import { ABOUT_OWNER_TITLE, DISCOUNT_OWNER_DISCRIPTION, COMMENTS } from "../../../Common/DummyData";
import Comments from "./Comments";

const AboutContent = (props) => {
  const ACCORDION_DATA = [
    {
      title: `${ABOUT_OWNER_TITLE}${props.title}`,
      content: <Text>{props.data.aboutClub}</Text>,
    },
    // { title: COMMENTS, content: <Comments /> }
  ];

  let IconDown = <Icon style={Styles.Icon} name="chevron-down" color={"black"} />;
  let IconLeft = <Icon style={Styles.Icon} name="chevron-left" color={"black"} />;
  const header = (item, expanded) => {
    return (
      <View style={Styles.Header}>
        <Text style={Styles.HeaderText}> {item.title}</Text>
        {expanded ? IconDown : IconLeft}
      </View>
    );
  };
  const renderContent = (item) => {
    return <View style={Styles.Content}>{item.content}</View>;
  };
  return (
    <View style={Styles.Container}>
      <Accordion
        style={Styles.Accordion}
        headerStyle={Styles.Header}
        dataArray={ACCORDION_DATA}
        renderHeader={header}
        renderContent={renderContent}
      />
    </View>
  );
};
export default AboutContent;
