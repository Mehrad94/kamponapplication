import { StyleSheet } from "react-native";
import {
  bgWhite,
  centerAll,
  Flex,
  fMainColor,
  fRow,
  h1,
  h2,
  h5,
  iranSans,
  pad8,
  posAbs,
  r8,
  fWhite,
  h4,
} from "../../../Values/Theme";
import { MAIN_COLOR } from "../../../Values/Colors";
const Styles = StyleSheet.create({
  Container: {
    ...Flex,
    // marginTop: 8,
    backgroundColor: "#eee",
  },
  Text: {
    ...iranSans,
    ...h5,
    textAlign: "right",
  },
  Header: {
    ...fRow,
    ...pad8,
    justifyContent: "flex-end",
    alignItems: "center",

    ...bgWhite,
    ...r8,
    margin: 5,
  },
  HeaderText: {
    ...h4,
    ...iranSans,
  },
  Icon: {
    ...h1,
    ...posAbs,
    left: 10,
  },
  Accordion: {
    backgroundColor: "transparent",
    marginVertical: 10,
  },
  Content: {
    marginHorizontal: 4,
    padding: 4,
    backgroundColor: "white",
  },
});
export default Styles;
