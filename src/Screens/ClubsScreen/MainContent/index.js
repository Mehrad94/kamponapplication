import React from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  PickerIOSComponent,
  ImageBackground,
} from "react-native";
import { Rating } from "react-native-ratings";
import Icon from "react-native-vector-icons/FontAwesome5";
import { fWhite, h5, iranSans, h6, h3 } from "../../../Values/Theme";
import Styles from "./styles";
import MApImage from "../../../Assets/Png/GoogleMapTA.png";
import { MAIN_COLOR } from "../../../Values/Colors";
const CARD_TITLE = "عضویت با کارت";
const MOBILE_TITLE = "عضویت با اپلیکیشن";
const PEOPLE_TITLE = "۴۵۰۰ عضو";
const ADDRESS_TITLE = "آدرس";
const PHONE_NUMBER = "تلفن";
const SHOW_IN_MAP = "نمایش روی نقشه";
const MainContent = (props) => {
  let Application = null;
  let card = null;
  let i;
  const { membership, percent } = props.data;

  const {
    coordinate,
    phone,
    isAvtive,
    balance,
    thumbnail,
    title,
    subTitle,
    address,
    phoneNumber,
    district,
    rating,
  } = props.data.owner;
  if (membership.includes("APPLICATION")) {
    Application = true;
  }
  if (membership.includes("CARD")) {
    card = true;
  }
  // console.log({ data });

  // MainContent.propTypes = {
  //   percent: PropTypes.string.isRequired
  // };
  // MainContent.defaultProps = {
  //   percent: "6"
  // };
  return (
    <View style={Styles.Container}>
      <View style={Styles.Content}>
        <View style={Styles.ContentRight}>
          <ImageBackground
            style={Styles.PercentHolder}
            source={require("../../../Assets/Png/discount.png")}
          >
            <Text style={[fWhite, iranSans, h5]}>{percent}%</Text>
          </ImageBackground>

          <Rating
            starContainerStyle={{ backgroundColor: "red" }}
            reviewColor="red"
            selectedColor="red"
            startingValue={rating}
            type="star"
            ratingColor="red"
            imageSize={20}
            ratingBackgroundColor="red"
            ratingCount={5}
            defaultRating={1}
            readonly={true}
            style={{ paddingVertical: 10, backgroundColor: "white" }}
          />
        </View>
        <View style={Styles.ContentLeft}>
          <Text style={[iranSans, h3]}>{props.data.title}</Text>
          <Text style={[Styles.Title, h5, { opacity: 0.5 }]}>{title}</Text>
        </View>
      </View>
      <View style={Styles.Icons}>
        <View style={Styles.IconsViewLeft}>
          <Icon name="credit-card" size={20} color={card ? MAIN_COLOR : "#d8d8d8"} />
          <Text style={[Styles.IconsTitle, card ? { color: MAIN_COLOR } : { color: "#d8d8d8" }]}>
            {CARD_TITLE}
          </Text>
        </View>
        <View style={Styles.IconsViewMiddle}>
          <Icon name="mobile-alt" size={20} color={Application ? MAIN_COLOR : "#d8d8d8"} />
          <Text
            style={[Styles.IconsTitle, Application ? { color: MAIN_COLOR } : { color: "#d8d8d8" }]}
          >
            {MOBILE_TITLE}
          </Text>
        </View>
        <View style={Styles.IconsViewRight}>
          <Icon name="users" size={20} color={MAIN_COLOR} />
          <Text style={[Styles.IconsTitle, { color: MAIN_COLOR }]}>{PEOPLE_TITLE}</Text>
        </View>
      </View>
      <View style={Styles.Details}>
        <View style={Styles.Address}>
          <Text style={[Styles.Text, { marginRight: 4 }]}>{address}</Text>
          <Text style={Styles.Text}>{ADDRESS_TITLE}:</Text>
        </View>
        <View style={Styles.Address}>
          {phone.map((phone, index) => (
            <Text key={index} style={[Styles.Text, { marginRight: 4 }]}>
              {phone}
            </Text>
          ))}

          <Text style={Styles.Text}>{PHONE_NUMBER}:</Text>
        </View>
      </View>
      <View style={Styles.MapView}>
        <Image source={MApImage} style={Styles.mapIAmge} />
        <Text style={Styles.MApTitle}>{SHOW_IN_MAP}</Text>
      </View>
    </View>
  );
};
export default MainContent;
