import { StyleSheet } from "react-native";
import { Flex, bgMainColor, accentColor, iranSans, h4, fWhite } from "../../Values/Theme";
import { MAIN_COLOR } from "../../Values/Colors";
const Styles = StyleSheet.create({
  Container: {
    ...Flex,
    backgroundColor: accentColor,
    padding: 8
  },
  Footer: {
    width: "100%",
    height: 50,
    backgroundColor: MAIN_COLOR,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center"
  },
  Text: {
    ...iranSans,
    ...h4,
    ...fWhite
  }
});
export default Styles;
