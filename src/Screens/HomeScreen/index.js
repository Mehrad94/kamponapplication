import React, { useContext, useEffect, useState } from "react";
import { ScrollView, StatusBar, View, Linking } from "react-native";
import Shimmer from "../../Components/Shimmer/Shimmer";
import { Context as DataContext } from "../../Context/DataContext";
import { MAIN_COLOR } from "../../Values/Colors";
import DiscountListContainer from "../Containers/DiscountListContainer";
import CategoryList from "../Lists/CategoryList";
import styles from "./styles";
import TopContentHome from "./TopContentHome";
import PushPole from "pushpole-react-native";
import CafeBazaarIntents from "react-native-cafebazaar-intents";

import { Context as CartContext } from "../../Context/CartScreenStepsContext";
const HomeScreen = () => {
  useEffect(() => {
    // Linking.openURL("https://cafebazaar.ir/app/com.hitalent.hitalent/?l=fa");
    // CafeBazaarIntents.showRatePackage("com.hitalent.hitalent").catch(() => {
    //   Linking.openURL("https://cafebazaar.ir/app/com.hitalent.hitalent/?l=fa");
    // });
  }, []);
  const cartContext = useContext(CartContext);
  const { setCartItems } = cartContext;
  // const [isVisible, setIsVisible] = useState(false);
  const context = useContext(DataContext);
  const { fetchAlldiscounts, fetchClubsData } = context;
  console.log({ mori: context.state });

  useEffect(() => {
    PushPole.addEventListener(PushPole.EVENTS.RECEIVED, (notification) => {
      console.log({ notification });
    });
    fetchAlldiscounts();
    fetchClubsData();
    setCartItems();
  }, []);

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={MAIN_COLOR} barStyle="light-content" />
      <ScrollView showsVerticalScrollIndicator={false} scrollEventThrottle={1}>
        <TopContentHome />
        <CategoryList />
        <DiscountListContainer />
      </ScrollView>
    </View>
  );
};

export default HomeScreen;
