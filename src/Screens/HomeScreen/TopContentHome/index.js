import React, { useEffect, useState, useContext } from "react";
import { View, Text, TouchableOpacity } from "react-native";
const TOP_TITLE = "دکتر تخفیف";
import Styles from "./styles";
import Icon from "react-native-vector-icons/FontAwesome5";
import { Input } from "native-base";
import { LIGHT_GRAY } from "../../../Values/Colors";
import { ActionSheet } from "native-base";
import {
  RASHT,
  LAHIJAN,
  ASTANE,
  REMOVE_FILETR,
  FILTER_ACCORDING_CITY,
  CITY,
  DIS_FILTER,
  CLUB_FILTER,
  FILTER_ACCORDING_TYPE,
  TYPE,
} from "../../../Common/Strings";
import { Context as DataContext } from "../../../Context/DataContext";
var DESTRUCTIVE_INDEX = 3;
var CANCEL_INDEX = 4;
const TopContentHome = () => {
  //============================== CONTEXT ==============================
  const context = useContext(DataContext);
  const { setCityFilter, setTypeFilter } = context;

  //============================== CONTEXT ==============================
  const [state, setState] = useState({ first: "", middle: "", end: "" });
  useEffect(() => {
    setCityFilter(state.end);
  }, [state.end]);
  useEffect(() => {
    setTypeFilter(state.middle);
  }, [state.middle]);
  const onFirstPressed = () => {
    const BUTTONS = ["morteza", "morteza 1", "morteza 2", "morteza 3", "morteza 4"];
    ActionSheet.show(
      {
        options: BUTTONS,
        cancelButtonIndex: CANCEL_INDEX,
        destructiveButtonIndex: DESTRUCTIVE_INDEX,
        title: "Testing ActionSheet",
      },
      (buttonIndex) => {
        setState({ ...state, first: BUTTONS[buttonIndex] });
      }
    );
  };
  const onMiddlePressed = () => {
    const BUTTONS = [DIS_FILTER, CLUB_FILTER, REMOVE_FILETR];
    ActionSheet.show(
      {
        options: BUTTONS,
        cancelButtonIndex: CANCEL_INDEX,
        destructiveButtonIndex: DESTRUCTIVE_INDEX,
        title: FILTER_ACCORDING_TYPE,
      },
      (buttonIndex) => {
        if (BUTTONS[buttonIndex] === REMOVE_FILETR) {
          setState({ ...state, middle: TYPE });
        } else {
          setState({ ...state, middle: BUTTONS[buttonIndex] });
        }
      }
    );
  };
  const onEndPressed = () => {
    const BUTTONS = [RASHT, LAHIJAN, ASTANE, REMOVE_FILETR];
    ActionSheet.show(
      {
        options: BUTTONS,
        cancelButtonIndex: CANCEL_INDEX,
        destructiveButtonIndex: DESTRUCTIVE_INDEX,
        title: FILTER_ACCORDING_CITY,
      },
      (buttonIndex) => {
        if (BUTTONS[buttonIndex] === REMOVE_FILETR) {
          setState({ ...state, end: CITY });
        } else {
          setState({ ...state, end: BUTTONS[buttonIndex] });
        }
      }
    );
  };
  return (
    <View style={Styles.Container}>
      <Text style={Styles.TopTitle}>{TOP_TITLE}</Text>
      <View style={Styles.Search}>
        <Icon name="search" color={LIGHT_GRAY} size={20} />
        <Input
          style={Styles.Input}
          placeholder="دنبال چی میگردی"
          placeholderTextColor={LIGHT_GRAY}
        />
      </View>
      <View style={Styles.DropDownView}>
        <TouchableOpacity style={Styles.DropDownd} onPress={onFirstPressed}>
          <Icon name="chevron-down" color={LIGHT_GRAY} />
          <Text style={Styles.DropDownText}>{state.first ? state.first : "تخفیف"}</Text>

          <Icon name="percentage" color={LIGHT_GRAY} />
        </TouchableOpacity>
        <TouchableOpacity style={Styles.DropDownd} onPress={onMiddlePressed}>
          <Icon name="chevron-down" color={LIGHT_GRAY} />
          <Text style={Styles.DropDownText}>{state.middle ? state.middle : TYPE}</Text>
          <Icon name="percentage" color={LIGHT_GRAY} />
        </TouchableOpacity>
        <TouchableOpacity style={Styles.DropDownd} onPress={onEndPressed}>
          <Icon name="chevron-down" color={LIGHT_GRAY} />
          <Text style={Styles.DropDownText}>{state.end ? state.end : CITY}</Text>

          <Icon name="percentage" color={LIGHT_GRAY} />
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default TopContentHome;
{
  /* <View style={Styles.DropDownsHolder}>
<ModalDropdown
  dropdownTextStyle={Styles.DropContentText}
  dropdownStyle={Styles.ContentDropDown}
  style={Styles.DropDownd}
  animated
  onSelect={i => console.log(i)}
  options={["option 1", "option 2"]}
  renderRow={i => (
    <View style={Styles.RowDropDown}>
      <Text style={Styles.DropContentText}>{i}</Text>
    </View>
  )}
>
  <View style={Styles.DropDownView}>
    <View style={Styles.EachDropElement}>
      <Icon name="chevron-down" color={LIGHT_GRAY} />
      <Text style={Styles.DropDownText}>تخفیف</Text>
      <Icon name="percentage" color={LIGHT_GRAY} />
    </View>
  </View>
</ModalDropdown>
<ModalDropdown
  dropdownStyle={Styles.ContentDropDown}
  style={Styles.DropDownd}
  animated
  onSelect={i => console.log(i)}
  options={["option 1", "option 2"]}
  renderRow={i => (
    <View style={Styles.RowDropDown}>
      <Text style={Styles.DropContentText}>{i}</Text>
    </View>
  )}
>
  <View style={Styles.DropDownView}>
    <View style={Styles.EachDropElement}>
      <Icon name="chevron-down" color={LIGHT_GRAY} />
      <Text style={Styles.DropDownText}>تخفیف</Text>
      <Icon name="percentage" color={LIGHT_GRAY} />
    </View>
  </View>
</ModalDropdown>
<ModalDropdown
  dropdownStyle={Styles.ContentDropDown}
  style={Styles.DropDownd}
  animated
  onSelect={i => console.log(i)}
  options={["option 1", "option 2"]}
  renderRow={i => (
    <View style={Styles.RowDropDown}>
      <Text style={Styles.DropContentText}>{i}</Text>
    </View>
  )}
>
  <View style={Styles.DropDownView}>
    <View style={Styles.EachDropElement}>
      <Icon name="chevron-down" color={LIGHT_GRAY} />
      <Text style={Styles.DropDownText}>تخفیف</Text>
      <Icon name="percentage" color={LIGHT_GRAY} />
    </View>
  </View>
</ModalDropdown>
</View> */
}
// {
//   ActionSheet.show(
//     {
//       options: BUTTONS,
//       cancelButtonIndex: CANCEL_INDEX,
//       destructiveButtonIndex: DESTRUCTIVE_INDEX,
//       title: "Testing ActionSheet"
//     },
//     (buttonIndex) => {
//       setState({ clicked: BUTTONS[buttonIndex] });
//     }
//   );
// }
