import { StyleSheet, Dimensions } from "react-native";
import {
  iranSans,
  Tac,
  h2,
  centerAll,
  h5,
  fRow,
  pad8,
  bgWhite,
  r8,
  posAbs,
  h1,
  fWhite,
} from "../../../Values/Theme";
import { H2 } from "native-base";
import { LIGHT_GRAY, MAIN_COLOR } from "../../../Values/Colors";
const SCREEN_WITH = Dimensions.get("window").width;
const Styles = StyleSheet.create({
  Container: {
    flex: 1,
    ...centerAll,
  },
  TopTitle: {
    ...iranSans,
    ...Tac,
    ...h2,
  },
  Search: {
    flexDirection: "row-reverse",
    width: SCREEN_WITH - 50,
    ...centerAll,
    backgroundColor: "white",
    borderRadius: 8,
    elevation: 1,
    paddingHorizontal: 8,
  },
  Input: {
    ...h5,
    ...iranSans,
    borderRadius: 8,
    textAlign: "right",
    // marginTop: 10,
  },
  DropDownView: {
    justifyContent: "space-between",
    alignItems: "center",
    ...fRow,
    marginTop: 8,
    width: SCREEN_WITH - 50,
  },
  DropDownText: {
    ...iranSans,
    ...h5,
    color: LIGHT_GRAY,
    marginHorizontal: 4,
  },
  EachDropElement: {
    alignItems: "center",
    justifyContent: "space-between",
    ...fRow,
    backgroundColor: "white",
    padding: 12,
    borderRadius: 8,

    width: SCREEN_WITH / 3.8,
  },
  DropDownsHolder: {
    ...fRow,
    width: SCREEN_WITH - 50,
    alignItems: "center",
    justifyContent: "center",
    elevation: 1,
  },
  DropDownd: {
    flexDirection: "row",
    width: SCREEN_WITH / 3.8,
    backgroundColor: "white",
    margin: 5,
    elevation: 1,
    borderRadius: 8,
    padding: 8,
    alignItems: "center",
    justifyContent: "center",
  },

  ContentDropDown: {
    width: SCREEN_WITH / 3.8,
    borderRadius: 8,
    backgroundColor: "transparent",
    borderWidth: 0,
  },
  DropContentText: {
    textAlign: "right",
    ...iranSans,
    margin: 4,
  },
  RowDropDown: {
    borderRadius: 8,
    backgroundColor: "white",
    margin: 1,
  },
});
export default Styles;
