import React, {useEffect} from 'react';
import {View, Text, Button} from 'react-native';
const HomeScreen = ({navigation}) => {
  console.log({navigation});

  return (
    <View>
      <Text>HomeScreen</Text>
      <Button title="gotoAuth" onPress={() => navigation.navigate('Sign In')} />
    </View>
  );
};
export default HomeScreen;
