import React, { useState, useEffect, useContext } from "react";
import { FlatList, View, Dimensions } from "react-native";
import ListRow from "./ListRow";
import { FLATLIST_DATA } from "../../Common/DummyData";
import BackgroundCursor from "../../Components/BackgroundCursor/BackgroundCursor";
import KamponTab from "./إCategoryTab";
// import ListRow from './ListRow-finished';
import { Context as DataContext } from "../../Context/DataContext";
import { Indicator } from "../../Components/Indicator";

const App = (props) => {
  const context = useContext(DataContext);
  const { state, fetchAlldiscounts, fetchClubsData } = context;
  if (!state.allDiscounts) {
    return <Indicator />;
  } else {
    return (
      <View style={{ flex: 1 }}>
        <BackgroundCursor images={FLATLIST_DATA} />
        <KamponTab />
      </View>
    );
  }
};
export default App;
