import { StyleSheet } from "react-native";

const Styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: "orange"
  },
  Image: {
    width: "100%",
    height: 150,
    backgroundColor: "green",
    margin: 8
  }
});
export default Styles;
