import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, Animated } from "react-native";
import Styles from "./styles";
const FlippRow = ({ item, scroll }) => {
  let koskesh = new Animated.Value(0);
  const [animation, setAnimation] = useState(new Animated.Value(0));
  let rotateIntepolate = koskesh.interpolate({
    inputRange: [0, 360],
    outputRange: ["0deg", "360deg"]
  });
  const animationStyles = {
    transform: [{ rotateY: rotateIntepolate }]
  };
  const startAnimation = () => {
    Animated.timing(koskesh, { toValue: 360, duration: 1500 }).start();
  };

  startAnimation();

  return (
    <View style={Styles.Container}>
      <TouchableOpacity>
        <Animated.Image
          source={{ uri: item.item }}
          style={[Styles.Image, animationStyles]}
        />
      </TouchableOpacity>
    </View>
  );
};
export default FlippRow;
{
}
