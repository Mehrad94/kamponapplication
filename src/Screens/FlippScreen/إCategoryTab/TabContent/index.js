import React, { useState } from "react";
import { View, FlatList, Text } from "react-native";
import { Flex, centerAll, iranSans, h4, mH16, mV8, h5 } from "../../../../Values/Theme";
import ListRow from "../../ListRow";
import SearchRow from "../../../Rows/SearchRow";

const TabContent = ({ data }) => {
  if (data.length > 0) {
    return (
      <FlatList
        initialNumToRender={5}
        data={data}
        renderItem={({ item, index }) => {
          if (!item.membership) {
            return <SearchRow searchItem={item} />;
          } else {
            return <ListRow item={item} index={index} />;
          }
        }}
        keyExtractor={(index) => Math.random(index * 1.57).toString()}
      />
    );
  } else {
    return (
      <View style={[centerAll, Flex]}>
        <Text style={[iranSans, h4]}>اطلاعاتی یافت نشد</Text>{" "}
      </View>
    );
  }
};

export default TabContent;

const KamponRow = ({ Rows }) => {
  return (
    <View style={[Flex, mH16, mV8]}>
      <Text style={[iranSans, h5]}>{Rows}</Text>
    </View>
  );
};
