import React, { useState, useContext } from "react";
import { View } from "react-native";
import { Flex, mV8 } from "../../../Values/Theme";
import TabContent from "./TabContent";
import Tabs from "./Tabs";
import { Context as DataContext } from "../../../Context/DataContext";
const KamponTab = () => {
  const context = useContext(DataContext);
  const { state } = context;
  const [tabActive, setTabActive] = useState(true);
  const [data, setData] = useState(state.allDiscounts);

  const _onTabClick = (isActive) => {
    {
      isActive ? setData(state.allClubsData) : setData(state.allDiscounts);
    }
  };

  return (
    <View style={[Flex, mV8]}>
      <Tabs onTabClick={_onTabClick} />
      <TabContent data={data} />
    </View>
  );
};

export default KamponTab;
