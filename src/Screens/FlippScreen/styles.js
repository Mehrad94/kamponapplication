import { StyleSheet } from "react-native";
import { screenWidth } from "../../Values/Constants";
import { iranSans, h5 } from "../../Values/Theme";

const Styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: "orange"
  },
  discountContainer: {
    // marginBottom: 8,
    // marginHorizontal: 4,/
    width: "100%",
    height: (screenWidth * 1.5) / 3,
    borderRadius: 5,
    backgroundColor: "white",
    elevation: 1,

    overflow: "hidden"
  },
  imgPlaceHolder: {
    width: "100%",
    height: screenWidth / 2
  },
  discountImage: {
    width: "100%",
    height: screenWidth / 3
  },

  discountTitleContainer: {
    textAlign: "center",
    marginLeft: 8,
    justifyContent: "center",
    fontFamily: "iran_sans"
  },

  discountPercentBg: {
    position: "absolute",
    width: 40,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    display: "flex",
    right: 0
  },

  discountPercent: {
    color: "white",
    fontSize: 13,
    fontFamily: "iran_sans"
  },

  discountPriceContainer: {
    position: "absolute",
    bottom: 0,
    right: 0,
    marginRight: 10,
    alignItems: "center"
  },
  sellCountContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  discountSellCount: {
    fontSize: 13,
    marginLeft: 8,
    paddingTop: 2,
    textAlignVertical: "center"
  },
  discountPriceReal: {
    color: "red",
    textAlign: "left",
    marginLeft: 8,
    fontSize: 13,
    textDecorationLine: "line-through",
    fontFamily: "iran_sans"
  },
  discountPriceNew: {
    color: "green",
    fontSize: 18,
    fontFamily: "iran_sans"
  },
  discountName: {
    textAlign: "left",
    fontSize: 16,
    width: "90%",
    fontFamily: "iran_sans"
  },

  discountPrices: {
    flexDirection: "row",
    //justifyContent: "sp",
    alignItems: "center"
  },
  Content: {
    flexDirection: "row-reverse",
    justifyContent: "space-between"
  },
  RightContent: {
    alignItems: "flex-end",
    marginTop: 4,
    marginRight: 6
    // justifyContent: "center"
  },
  Text: {
    ...iranSans,
    ...h5
  },
  Icons: {
    // flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-between",
    marginLeft: 16,
    marginBottom: 16,
    width: "30%",
    height: 50
  },
  Icon: {
    flex: 1,
    flexDirection: "row",
    position: "absolute",
    bottom: -5,
    left: -5
  },
  Stars: {
    position: "absolute",
    top: 5,
    right: 10
  },
  RatingHolder: {
    backgroundColor: "red",
    justifyContent: "flex-start",
    alignItems: "flex-start"
  },
  row: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 4
    // marginBottom: 12s
    // paddingTop: 24
    // marginTop: 8
  },
  image: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 10
  },
  name: {
    fontSize: 18,
    fontWeight: "500"
  },
  email: {
    fontSize: 14
  }
});
export default Styles;
