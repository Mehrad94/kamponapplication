import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  Animated,
  TouchableOpacity,
  Dimensions
} from "react-native";

const ANIMATION_DURATION = 1500;
const ROW_HEIGHT = (screenWidth * 1.5) / 3;
import styles from "./styles";
import { bgWhite } from "../../Values/Theme";
import FastImage from "react-native-fast-image";
import { Rating } from "react-native-ratings";
import Icon from "react-native-vector-icons/FontAwesome5";
import { screenWidth } from "../../Values/Constants";
import { navigate } from "../../../navigationRef";
const ListRow = (props) => {
  let _animated = new Animated.Value(0);
  let show = new Animated.Value(0);
  const [position, setPosition] = useState(_animated);
  const [showPosition, setShowPosition] = useState(show);

  useEffect(() => {
    Animated.timing(show, {
      toValue: 1,
      duration: 1000
    }).start();
  }, []);

  const onRemove = () => {
    console.log("remove");
    navigate("ClubsScreen", { clubParams: props.item });
    Animated.timing(_animated, {
      toValue: 360,
      duration: ANIMATION_DURATION
    }).start(() => _animated.setValue(0));
  };

  const rowStyles = [styles.row];

  return (
    <TouchableOpacity onPress={onRemove} activeOpacity={0.8}>
      <View style={rowStyles}>
        <View style={styles.discountContainer}>
          <View style={[bgWhite]}>
            <FastImage
              style={styles.discountImage}
              source={{ uri: props.item.owner.thumbnail }}
              resizeMode={FastImage.resizeMode.cover}
            />
            <View style={styles.Content}>
              <View style={styles.RightContent}>
                <Text style={styles.Text}>مرکز خرید اسکار</Text>
                <Text style={styles.Text}>خرید اسکار</Text>
              </View>
              <View style={styles.Icons}>
                <View style={styles.RatingHolder}>
                  <Rating
                    startingValue={2}
                    type="star"
                    imageSize={20}
                    ratingCount={5}
                    defaultRating={1}
                    readonly={true}
                    //   onFinishRating={this.ratingCompleted}
                    style={styles.Stars}
                  />
                </View>

                <View style={styles.Icon}>
                  <Icon name="credit-card" size={20} color="#d8d8d8" style={{ marginRight: 12 }} />
                  <Icon name="mobile-alt" size={20} color="#d8d8d8" style={{ marginRight: 12 }} />
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default ListRow;
// {
//   height: show.interpolate({
//     inputRange: [0, ROW_HEIGHT],
//     outputRange: [-ROW_HEIGHT, ROW_HEIGHT],
//     extrapolate: "clamp"
//   })
// },
// { opacity: show },
// {
//   transform: [
//     //   { scale: this._animated },
//     {
//       rotateY: _animated.interpolate({
//         inputRange: [0, 360],
//         outputRange: ["0deg", "360deg"],
//         extrapolate: "clamp"
//       })
//     }
//   ]
// }
