import { StyleSheet } from "react-native";
import { MAIN_COLOR } from "../../../Values/Colors";
import { screenWidth } from "../../../Values/Constants";
import { iranSans } from "../../../Values/Theme";
import { TEXT_COLOR } from "../../../Common/Colors";

export default styles = StyleSheet.create({
  categoryContainer: {
    marginHorizontal: 4,
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
    borderRadius: 8,
    marginTop: 16,
    padding: 8
  },

  categoryTitleParent: {
    alignItems: "center"
  },
  categoryTitle: {
    fontSize: 12,
    ...iranSans,
    marginTop: 4,
    color: TEXT_COLOR
  },
  Image: {
    width: screenWidth / 10,
    height: screenWidth / 10
  }
});
