import React, { useEffect, useState } from "react";
import { Text, StyleSheet, TouchableWithoutFeedback, View, Image } from "react-native";
import ShimmerCategory from "../../../Components/Shimmer/ShimmerCategory";
import { screenWidth } from "../../../Values/Constants";
import { catHome } from "../../../Values/Theme";
import Icon from "react-native-vector-icons/FontAwesome5";
import styles from "./styles";

const CategoryRow = (props) => {
  const onCategoryPress = () => {
    props.onCategoryPress(props.cat.catTitle, props.cat._id);
  };
  const { image, titleFa } = props.cat;

  return (
    <TouchableWithoutFeedback onPress={onCategoryPress}>
      <View style={styles.categoryContainer}>
        <Image source={{ uri: image }} style={styles.Image} />
        <Text style={styles.categoryTitle}>{titleFa}</Text>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default CategoryRow;
const styless = StyleSheet.create({
  image: {
    width: screenWidth / 3,
    height: screenWidth / 5
  }
});
