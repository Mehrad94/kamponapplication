import { StyleSheet, Dimensions } from "react-native";
import Constants from "../../../Values/Constant";
import { screenWidth } from "../../../Values/Constants";
import { centerAll, iranSans, h5 } from "../../../Values/Theme";
import { TEXT_COLOR } from "../../../Common/Colors";

const style = StyleSheet.create({
  discountContainer: {
    marginTop: 8,
    marginBottom: 8,
    marginHorizontal: 4,
    width: screenWidth / 2,
    // height: (screenWidth * 1.5) / 3.4,
    borderRadius: 5,
    elevation: 1,

    overflow: "hidden",
  },
  imgPlaceHolder: {
    width: "100%",
    height: screenWidth / 2,
  },
  discountImage: {
    width: "100%",
    height: screenWidth / 3.5,
  },

  discountTitleContainer: {
    textAlign: "center",
    marginLeft: 8,
    justifyContent: "center",
    fontFamily: "iran_sans",
  },

  discountPercentBg: {
    position: "absolute",
    width: 40,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    display: "flex",
    right: 0,
  },

  discountPercent: {
    color: "white",
    fontSize: 13,
    fontFamily: "iran_sans",
  },

  discountPriceContainer: {
    position: "absolute",
    bottom: 0,
    right: 0,
    marginRight: 10,
    alignItems: "center",
  },
  sellCountContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  discountSellCount: {
    fontSize: 13,
    marginLeft: 8,
    paddingTop: 2,
    textAlignVertical: "center",
  },
  discountPriceReal: {
    color: "red",
    textAlign: "left",
    marginLeft: 8,
    fontSize: 13,
    textDecorationLine: "line-through",
    fontFamily: "iran_sans",
  },
  discountPriceNew: {
    fontSize: 18,
    fontFamily: "iran_sans",
  },
  discountName: {
    textAlign: "left",
    fontSize: 16,
    width: "90%",
    fontFamily: "iran_sans",
  },

  discountPrices: {
    flexDirection: "row",
    //justifyContent: "sp",
    alignItems: "center",
  },
  Content: {
    flexDirection: "row-reverse",
    justifyContent: "space-between",
    marginBottom: -10,
  },
  RightContent: {
    alignItems: "flex-end",
    marginTop: 4,
    marginRight: 6,
    // justifyContent: "center"
  },
  Text: {
    ...iranSans,
    ...h5,
    color: TEXT_COLOR,
    lineHeight: 20,
  },
  Icons: {
    alignItems: "flex-end",
    marginTop: 4,
    marginLeft: 6,
  },

  PriceHolder: {
    justifyContent: "flex-end",
    alignItems: "flex-start",
  },
});

export default style;
