import React, { useState, useContext, useEffect } from "react";
import {
  TextInput,
  Image,
  KeyboardAvoidingView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  ToastAndroid,
  Dimensions,
  ActivityIndicator
} from "react-native";
import evvelope from "../../Assets/Png/checked.png";
import { MAIN_COLOR } from "../../Common/Colors";

import {
  bgMainColor,
  centerAll,
  Flex,
  fWhite,
  h3,
  h4,
  iranSans,
  mH32,
  Tac,
  mV4,
  bgWhite,
  mH8,
  mV8,
  mV16,
  pad16,
  h5,
  fRow
} from "../../Values/Theme";
import { Context as AuthContext } from "../../Context/AuthContext";
import TokenScreen from "./TokenScreen/TokenScreen";
import { screenHeight, screenWidth } from "../../Values/Constants";

const mobilenumberlable = "شماره همراه";
const entertoapptext = "ورود به برنامه ";
const LodinScreen = ({ navigation, H1, type }) => {
  const { state, signup, editNumber, clearErrorMessage } = useContext(AuthContext);
  const { loading, errorMessage } = state;
  const [rightLable, setRightLable] = useState(false);
  const [inputValue, setInputValue] = useState("");

  const Inputs = () => {
    switch (type) {
      case "Login":
        return (
          <View>
            <TextInput
              placeholder={"09 - -  - - -  - - - -"}
              maxLength={11}
              value={inputValue}
              onChangeText={_changeMobileInput}
              keyboardType={"numeric"}
              textAlignVertical={"center"}
              onBlur={() => setRightLable(false)}
              onFocus={() => setRightLable(true)}
              style={[
                pad16,
                iranSans,
                {
                  height: screenHeight / 12,
                  width: screenWidth / 1.5,
                  borderBottomWidth: 0,
                  borderRadius: 20,
                  direction: "rtl",
                  textAlign: "center",

                  backgroundColor: "rgba(64,199,133,0.2)"
                }
              ]}
            />
            <TouchableOpacity
              onPress={() => signup(inputValue)}
              style={[
                centerAll,
                {
                  alignSelf: "center",
                  borderRadius: 20,
                  marginTop: 50,
                  width: 200,
                  height: 50,
                  backgroundColor: "red"
                }
              ]}
            >
              {loading ? (
                <ActivityIndicator size={40} color="white" />
              ) : (
                <Text style={[iranSans, fWhite, h4]}>{entertoapptext}</Text>
              )}
            </TouchableOpacity>

            {errorMessage ? (
              <Text style={[iranSans, h4, mV8, { color: "red", textAlign: "center" }]}>
                {errorMessage}
              </Text>
            ) : null}
          </View>
        );
      case "Token":
        return <TokenScreen mobileNumber={inputValue} navigation={navigation} />;
      default:
        return null;
    }
  };
  const _changeMobileInput = (text) => {
    let newText = "";
    let numbers = "0123456789";
    for (let i = 0; i < text.length; i++) {
      if (numbers.indexOf(text[i]) > -1) {
        if (text.charAt(0) != "0")
          return ToastAndroid.show("شماره همراه اشتباه است", ToastAndroid.SHORT);
        if (text.length > 1 && text.charAt(1) != "9")
          return ToastAndroid.show("شماره همراه اشتباه است", ToastAndroid.SHORT);

        newText = newText + text[i];
      } else return ToastAndroid.show("شماره همراه اشتباه است", ToastAndroid.SHORT);
    }
    setInputValue(newText);
  };

  return (
    <View style={[Flex, bgMainColor]}>
      <KeyboardAvoidingView style={[Flex]} behavior="position" enabled>
        <View style={[{ width: "100%", height: screenHeight / 2 }, bgMainColor, centerAll]}>
          <Image
            source={evvelope}
            style={[{ width: screenWidth / 2.7, height: screenWidth / 3 }]}
          />
          <Text style={[iranSans, h3, fWhite, Tac, mH32]}>{H1}</Text>
          {type === "Token" ? <Text style={[iranSans, mV8, fWhite]}>{inputValue}</Text> : null}
          {type === "Token" ? (
            <TouchableOpacity
              onPress={() => {
                editNumber(), clearErrorMessage();
              }}
            >
              <Text style={[iranSans, h5, fWhite, { textDecorationLine: "underline" }]}>
                ویرایش شماره
              </Text>
            </TouchableOpacity>
          ) : null}
        </View>
        <View
          style={[
            bgWhite,
            centerAll,
            {
              width: "100%",
              height: screenHeight / 2,
              borderTopLeftRadius: 40,
              borderTopRightRadius: 40
            }
          ]}
        >
          {Inputs()}
        </View>
      </KeyboardAvoidingView>
    </View>
  );
};
LodinScreen.navigationOptions = () => {
  return {
    header: null
  };
};
export default LodinScreen;
