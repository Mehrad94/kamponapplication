import React from "react";
import { FlatList, View, Text } from "react-native";
import CartItem from "../../CartScreen/Cart/";
import { Indicator } from "../../../Components/Indicator";
import { Flex, centerAll, iranSans, h3 } from "../../../Values/Theme";
import { NO_ITEM_IN_CART } from "../../../Common/Strings";

const CartItemList = (props) => {
  const renderCartItem = (item) => {
    return <CartItem data={item} />;
  };
  if (props.items.length < 1) {
    return (
      <View style={[Flex, centerAll]}>
        <Text style={[iranSans, h3]}>{NO_ITEM_IN_CART}</Text>
      </View>
    );
  }
  return (
    <View style={{ marginBottom: 95 }}>
      <FlatList
        data={props.items}
        renderItem={renderCartItem}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
};
export default CartItemList;
