import React, { PureComponent, useMemo, createRef, useReducer } from "react";
import { FlatList, Dimensions, View, Button } from "react-native";
import DiscountRow from "../../Rows/DiscountRow";

const DiscountList = props => {
  // _discountClick = (id, cat) => {
  //   this.props.discounOnClick(id, cat);
  // };
  // console.log("render discountsList");

  const flatListRef = createRef();
  const renderItem = ({ item }) => {
    return <DiscountRow rows={item} />;
  };
  const ITEM_HEIGHT = Dimensions.get("window").width;
  const getItemLayout = (data, index) => ({
    length: ITEM_HEIGHT,
    offset: (ITEM_HEIGHT * index) / 2,
    index
  });
  return useMemo(() => {
    return (
      <View style={{ flex: 1 }}>
        <FlatList
          endFillColor="red"
          ListHeaderComponentStyle={{
            borderRadius: 100,
            width: 30,
            height: 20
          }}
          // ListHeaderComponent={() => {
          //   return (
          //     <Button
          //       onPress={() => flatListRef.current.scrollToEnd()}
          //       title="tt"
          //     />
          //   );
          // }}
          ref={flatListRef}
          contentOffset={{ x: 40, y: 40 }}
          legacyImplementation={true}
          windowSize={50}
          initialNumToRender={5}
          maxToRenderPerBatch={10}
          inverted
          getItemLayout={getItemLayout}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          data={props.contents}
          renderItem={renderItem}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }, [props.contents]);
};
export default DiscountList;
