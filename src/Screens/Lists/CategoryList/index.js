import React, { useContext, useMemo } from "react";
import { FlatList, View } from "react-native";
import { Flex, mV8 } from "../../../Values/Theme";
import CategoryRow from "../../Rows/CategoryRow";
import { Context as DataContext } from "../../../Context/DataContext";
import styles from "./styles";
import { navigate } from "../../../../navigationRef";

const CategoryList = () => {
  //======================== CONTEXT=================
  const context = useContext(DataContext);
  const { state, fetchCategoriesData } = context;
  const data = state.categories;

  //======================== CONTEXT=================

  const onPress = (title, id) => {
    fetchCategoriesData(id);
    navigate("CategoriesScreen");
  };

  const renderItem = ({ item }) => {
    return <CategoryRow onCategoryPress={onPress} cat={item} />;
  };

  return useMemo(() => {
    return (
      <View style={styles.container}>
        <FlatList
          inverted
          showsHorizontalScrollIndicator={false}
          horizontal
          data={data}
          renderItem={renderItem}
          keyExtractor={(item, index) => Math.random(Math.pow(index) * 3.5).toString()}
        />
      </View>
    );
  }, [data]);
};
export default CategoryList;
