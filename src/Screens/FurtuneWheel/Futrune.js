import React, { useEffect, useState } from "react";
import { View, StyleSheet, Animated, Dimensions } from "react-native";
import { centerAll } from "../../Values/Theme";
import * as d3Shape from "d3-shape";
import Svg, { G, Text, TSpan, Path, Image, Circle, ClipPath, Defs } from "react-native-svg";
const AnimatedSvg = Animated.createAnimatedComponent(Svg);

const WheelOfFortune = (props) => {
  // ========================  VARIABLES ==================
  const animAngle = new Animated.Value(0);
  const angle = 0;
  const Rewards = props.rewards;
  const RewardCount = Rewards.length;
  const numberOfSegments = RewardCount;
  const fontSize = 20;
  const oneTurn = 360;
  const angleBySegment = oneTurn / numberOfSegments;
  const angleOffset = angleBySegment / 2;
  const winner = props.winner ? props.winner : Math.floor(Math.random() * numberOfSegments);
  const makeWheel = () => {
    const data = Array.from({ length: numberOfSegments }).fill(1);
    // console.log({ MOriData: data });

    const arcs = d3Shape.pie()(data);
    const colors = ["red", "blue"];
    const Rewards = props.rewards;
    console.log({ moriArc: arcs[0] });
    const mehrad = d3Shape
      .arc()
      .padAngle(0.01)
      .outerRadius(width / 2)
      .innerRadius(100);
    console.log({ mehrad: mehrad.centroid(arcs[0]) });

    return arcs.map((arc, index) => {
      const instance = d3Shape
        .arc()
        .padAngle(0.01)
        .outerRadius(width / 2)
        .innerRadius(100);
      console.log({ MORI: instance.centroid(arc) });

      return {
        path: instance(arc),
        color: colors[index % colors.length],
        value: Rewards[index],
        centroid: instance.centroid(arc)
      };
    });
  };
  const wheelPaths = makeWheel();
  console.log({ wheelPaths: wheelPaths });

  props.onRef();
  useEffect(() => {
    // console.log({ wheelPaths });
  });
  const { width, height } = Dimensions.get("screen");
  const [state, setState] = useState({
    enabled: false,
    started: false,
    finished: false,
    winner: null,
    gameScreen: new Animated.Value(width - 40),
    wheelOpacity: new Animated.Value(1),
    imageLeft: new Animated.Value(width / 2 - 30),
    imageTop: new Animated.Value(height / 2 - 70)
  });
  // ========================  VARIABLES ==================

  const onPress = () => {
    const duration = props.duration || 10000;

    setState({ ...state, started: true });
    Animated.timing(animAngle, {
      toValue: 365 - winner * (oneTurn / numberOfSegments) + 360 * (duration / 1000),
      duration: duration,
      useNativeDriver: true
    }).start(() => {
      const winnerIndex = getwinnerIndex();
      setState({ ...state, finished: true, winner: wheelPaths[winnerIndex].value, started: false });
      props.getWinner(wheelPaths[winnerIndex].value, winnerIndex);
    });
  };

  const getwinnerIndex = () => {
    const deg = Math.abs(Math.round(angle % oneTurn));
    // wheel turning counterclockwise
    if (angle < 0) {
      return Math.floor(deg / angleBySegment);
    }
    // wheel turning clockwise
    return (numberOfSegments - Math.floor(deg / angleBySegment)) % numberOfSegments;
  };
  const renderSvgWheel = () => {
    return (
      <View style={styles.container}>
        {/* {_renderKnob()} */}
        <Animated.View
          style={{
            alignItems: "center",
            justifyContent: "center",
            transform: [
              {
                rotate: animAngle.interpolate({
                  inputRange: [-oneTurn, 0, oneTurn],
                  outputRange: [`-${oneTurn}deg`, `0deg`, `${oneTurn}deg`]
                })
              }
            ],
            backgroundColor: props.backgroundColor ? props.backgroundColor : "#fff",
            width: width - 20,
            height: width - 20,
            borderRadius: (width - 20) / 2,
            borderWidth: props.borderWidth ? props.borderWidth : 2,
            borderColor: props.borderColor ? props.borderColor : "#fff",
            opacity: state.wheelOpacity
          }}
        >
          <AnimatedSvg
            width={state.gameScreen}
            height={state.gameScreen}
            viewBox={`0 0 ${width} ${width}`}
            style={{
              transform: [{ rotate: `-${angleOffset}deg` }],
              margin: 10
            }}
          >
            <G y={width / 2} x={width / 2}>
              {wheelPaths.map((arc, i) => {
                const [x, y] = arc.centroid;

                return (
                  <G key={`arc-${i}`}>
                    <Path d={arc.path} strokeWidth={2} fill={arc.color} />
                    <G
                      rotation={(i * oneTurn) / numberOfSegments + angleOffset}
                      origin={`${x}, ${y}`}
                    >
                      {typeof arc.value === "object"
                        ? imageRender(x, y, arc.value, 60)
                        : textRender(x, y, arc.value, 60, i)}
                    </G>
                  </G>
                );
              })}
            </G>
          </AnimatedSvg>
        </Animated.View>
      </View>
    );
  };
  const imageRender = (x, y, value, size) => {
    return (
      <Svg height="60" width="60">
        <Defs>
          <ClipPath id="clip">
            <Circle x={x} y={y} r={size / 2} />
          </ClipPath>
        </Defs>
        <Image
          x={x - size / 2}
          y={y - size / 2}
          width={size}
          height={size}
          preserveAspectRatio="xMidYMid slice"
          opacity="1"
          href={value}
          clipPath="url(#clip)"
        />
      </Svg>
    );
  };

  const textRender = (x, y, value, size, i) => {
    console.log({ x, y, value, size, i });

    // return (
    //   <Text
    //     x={x}
    //     y={y - size}
    //     fill={props.textColor ? props.textColor : "#fff"}
    //     textAnchor="middle"
    //     fontSize={fontSize}
    //   >
    //     {Array.from({ length: value.length }).map((_, j) => {
    //       return (
    //         <TSpan x={x} dy={fontSize} key={`arc-${i}-slice-${j}`}>
    //           {value.toString().charAt(j)}
    //         </TSpan>
    //       );
    //     })}
    //   </Text>
    // );
  };

  return (
    <View style={styles.container}>
      <Animated.View style={[styles.content, { padding: 10 }]}>{renderSvgWheel()}</Animated.View>
    </View>
  );
};
export default WheelOfFortune;
const styles = StyleSheet.create({
  container: { flex: 1, ...centerAll }
});
