import React, { Component } from "react";
import { View, Text, StyleSheet, StatusBar, Button } from "react-native";

import WheelOfFortune from "./Futrune";
import { MAIN_COLOR } from "../../Values/Colors";
const rewardString = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"];
const participants = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const rewards = participants.map((e) => ({
  uri: `https://i.pravatar.cc/300?$1`,
  item: e
}));
const colors = ["white", "gray", MAIN_COLOR, "red"];
class FurtuneWheel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      winnerValue: null,
      winnerIndex: null
    };
    this.child = null;
  }

  _renderPlayButton = () => {
    return <Text style={styles.tapToStart}>TAP TO PLAY</Text>;
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle={"light-content"} />
        <WheelOfFortune
          colors={colors}
          onRef={(ref) => (this.child = ref)}
          rewards={rewardString}
          knobSize={32}
          borderWidth={3}
          borderColor={"#FFF"}
          innerRadius={10}
          duration={5000}
          backgroundColor={"#c0392b"}
          // winner={2}
          // getWinner={(value, index) => this.setState({ winnerValue: value, winnerIndex: index })}
        />
        {/* <Button
          title="Press me"
          onPress={() => {
            this.child._onPress();
          }}
        /> */}
        {/* <Text>{this.child.getWinner()}</Text> */}
      </View>
    );
  }
}
export default FurtuneWheel;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
    // backgroundColor: "#E74C3C"
  },
  winner: {
    width: "100%",
    position: "absolute",
    padding: 10,
    backgroundColor: "#fff",
    bottom: 50,
    justifyContent: "center",
    alignItems: "center"
  },
  winnerText: {
    fontSize: 26,
    color: "#666"
  },
  tapToStart: {
    fontSize: 50,
    color: "#fff",
    fontWeight: "bold"
  }
});
