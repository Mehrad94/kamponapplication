import React, { useState, useEffect, useContext } from "react";
import { Context as DataContext } from "../../Context/DataContext";
import { View, Image, Linking, Platform } from "react-native";
import VersionNumber from "react-native-version-number";
import { checkTokenInDevice } from "../../API/checkTokenInDevice";
import logo from '../../Assets/Png/drclubs.png'
import styles from "./styles";
const SplashScreen = ({ navigation }) => {
  const context = useContext(DataContext);
  const { state, fetchCaregories, fetchSplashData } = context;
  useEffect(() => {
    fetchCaregories();
    checkUserAndFetchData();
    // navigation.navigate("Tabs");
  }, []);
  const checkUserAndFetchData = async () => {
    const token = await checkTokenInDevice();
    if (token) {
      if (Platform.OS === "android") {
        Linking.getInitialURL().then((url) => {
          console.log({ url });

          if (url) {
            const resUrlSplit = url.split("/");
            const page = resUrlSplit[3];
            const Id = resUrlSplit[4];
            switch (page) {
              case "cart":
                navigation.navigate("CartScreen", { ticket: true });
                break;
              case "discount":
                navigation.navigate("Discount", { disId: Id });
                break;
            }
          } else {
            fetchSplashData(() => navigation.navigate("Tabs"));
          }
        });
      }
    } else {
      navigation.navigate("AuthFlow");
    }
  };
  return (
    <View style={styles.container}>
       <Image source={logo} style ={styles.img}/>
    </View>
  );
};

export default SplashScreen;
