import {StyleSheet} from "react-native";
import {screenHeight, screenWidth} from "../../Values/Constants";
import { mainColor } from "../../Common/Colors";

export default styles = StyleSheet.create({
    container: {
        justifyContent: "center",
        alignItems: "center",
        flex: 1,
        backgroundColor: mainColor
    },
    img: {
        width: screenWidth /1.5,
        height: screenWidth / 2
    }
});
