import React, {useState, useEffect} from 'react';
import {View, Text, Image, TouchableOpacity, ScrollView} from 'react-native';
import Styles from './styles';
import logo from '../../../Assets/Png/drclubs.png'

const AboutUs = ({navigation}) => {
  const text1 = 'در نرم افزار دکــتر کــلابــز می توانید از اطلاعات ، تخفیفات و حراجی های فروشگاههای شهر خود مطلع شده و در دکــتر کلابز استور می توانید سفارشات و محصولات مورد نیاز خود را به صورت مستقیم از فروشگاههای طرف قراداد خرید نمایید.'
const text2 = 'نرم افزار دکـتر کـلابــز یک سـرویس خدمـات شـهری می باشد که با هدف ترویج هر چه بیشتر خدمات دهندگان از طریق بستر اینترنت تلاش به افزایش رضایتمندی مشتریان به واسط رتبه بندی کسب و کارها ، انتقال نظرات و پیشنهادات شما به صاحبان کسب و کار دارد.'
  const text3 = 'اگر شما مالک و صاحب کسب و کار فروشگاهی یا خدماتی هستید می توانید با پیوستن به باشگاه مدیران برتر دکتر کلابز خدمات خود را از طریق بستر نرم افزار دکتر کلابز در دسترس هزاران نفر مشتری در سطح شهر، استان و کشور قرار دهید.'
  const text4 = 'برای بهبود سرویس های کسب و کارهای فعال در نرم افزار لطفاً نظرات خود را پس از دریافت خدمات با دیگر کاربران نرم افزار در میان گذاشته و به کسب و کارهای محبوب خود امتیاز دهید.'
  const titleText = 'درباره دکــتر کــلابز'
  const endText = 'مدیریت هوشمند مشتریان دکتر کلابز'
  return (
    <View style={Styles.Container}>
    
      <View style={Styles.TopBack}>
      <Image source={logo} style ={Styles.img}/>
      </View>
      <ScrollView style={Styles.ViewCenter}>
      <Text style={Styles.Text}>{titleText}</Text>
  <Text style={Styles.TextInput}>{text1}</Text>
  <Text style={Styles.TextInput}>{text2}</Text>
  <Text style={Styles.TextInput}>{text3}</Text>
  <Text style={Styles.TextInput}>{text4}</Text>
  <Text style={Styles.Text}>{endText}</Text>
      </ScrollView>
    </View>
  );
};

export default AboutUs;
