import {StyleSheet, Dimensions} from 'react-native';
const {height, width} = Dimensions.get('screen');
import {mainColor} from '../../../Common/Colors';

const Styles = StyleSheet.create({
  Container: {
    flex: 1,
  },
  TopBack: {
    height: width,
    backgroundColor: mainColor,
    borderBottomStartRadius: 50,
    borderBottomEndRadius: 50,
  },
  Text: {
    fontFamily: 'iran_sans',
    fontSize:20,
    textAlign:'center',
    marginTop: 10,
    paddingHorizontal:10,
  },

  ViewCenter: {
    position: 'absolute',
    width: width/1.1,
    height: width * 1.3,
    backgroundColor: '#e8e8e8',
    borderRadius: 25,
    alignSelf: 'center',
    top: width / 2.5,
  },



  Number: {
    flexDirection: 'row',
    paddingVertical: 20,
    paddingHorizontal: 15,
  },
  TextInput: {
    fontFamily: 'iran_sans',
    paddingHorizontal:10,
    marginTop: 10,
    textAlign:'center',
    fontSize:12
  },
  img: {
    height:width/3,
    alignSelf: 'center',
    marginVertical:10
  }
});
export default Styles;
