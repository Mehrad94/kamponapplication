import React, { useEffect, useContext } from "react";
import { View, Text, ScrollView, TouchableOpacity, FlatList, Image, Linking } from "react-native";
import Styles from "./styles";
import Icon from "react-native-vector-icons/FontAwesome5";
import {
  USER_INFO,
  MY_KAMPONS,
  MY_TRANSACTIONS,
  TERM_OF_USE,
  COMMON_QUESTIONS,
  CONNECT_WITH_US,
  ABOUT_US,
  EXIT_USER,
  YOUR_MENTIONS,
  SUPPORT,
} from "../../Common/Strings";
import Thumnail from "../../Assets/Images/ic_logo.png";
import { navigate } from "../../../navigationRef";
import { Context as UserDataContext } from "../../Context/userDataContext";
import { iranSans, h3, fWhite } from "../../Values/Theme";
import CafeBazaarIntents from "react-native-cafebazaar-intents";
const ProfileScreen = () => {
  const context = useContext(UserDataContext);
  const { state, getMemberInformation } = context;
  useEffect(() => {
    getMemberInformation();
  }, []);
  const data = [
    { iconName: "user", title: USER_INFO, route: "" },
    { iconName: "calendar-day", title: MY_KAMPONS, route: "KamponScreen" },
    { iconName: "bookmark", title: "علاقه مندی ها", route: "Bookmarks" },
    { iconName: "chart-bar", title: MY_TRANSACTIONS, route: "" },
    { iconName: "book-open", title: TERM_OF_USE, route: "" },
    { iconName: "user", title: COMMON_QUESTIONS, route: "" },
    { iconName: "teamspeak", title: CONNECT_WITH_US, route: "ContactUs" },
    { iconName: "user", title: ABOUT_US, route: "AboutUs" },
    { iconName: "comment-alt", title: YOUR_MENTIONS, route: "mention" },
    { iconName: "user-check", title: SUPPORT, route: "" },
    { iconName: "sign-in-alt", title: EXIT_USER, route: "" },
  ];
  const lastindex = data.length;
  console.log({ lastindex });
  console.log({ state });
  let avatar = Thumnail;
  if (state.userInfo) {
    avatar = { uri: state.userInfo.avatar };
  }
  const gotoBazar = async () => {
    Linking.openURL("https://cafebazaar.ir/app/ir.kampon.www.kampon/?l=fa");
    // CafeBazaarIntents.showRatePackage("ir.kampon.www.kampon").catch(() => {
    //   Linking.openURL("https://cafebazaar.ir/app/ir.kampon.www.kampon/?l=fa");
    // });
  };
  const onPressMiddle = (route) => {
    if (route === "mention") {
      gotoBazar();
    } else {
      navigate(route);
    }
  };
  const renderItem = ({ item, index }) => {
    if (index === 0) {
      return (
        <TouchableOpacity onPress={() => navigate(item.route)} style={Styles.firsRows}>
          <Icon name={item.iconName} size={30} color="#aaa" />
          <Text style={Styles.text}>{item.title}</Text>
        </TouchableOpacity>
      );
    } else if (index === lastindex - 1) {
      return (
        <TouchableOpacity onPress={() => navigate(item.route)} style={Styles.lastRows}>
          <Icon name={item.iconName} size={30} color="#aaa" />
          <Text style={Styles.text}>{item.title}</Text>
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity onPress={() => onPressMiddle(item.route)} style={Styles.rows}>
          <Icon name={item.iconName} size={30} color="#aaa" />
          <Text style={Styles.text}>{item.title}</Text>
        </TouchableOpacity>
      );
    }
  };
  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      scrollEventThrottle={1}
      renderToHardwareTextureAndroid={true}
    >
      <View style={Styles.container}>
        <View style={Styles.topView}>
          <Image source={avatar} style={Styles.image} />
          <Text style={[iranSans, h3, fWhite]}>morteza</Text>
        </View>
        <View style={Styles.mainView}>
          <FlatList
            data={data}
            renderItem={renderItem}
            keyExtractor={(item, index) => item.title}
          />
        </View>
      </View>
    </ScrollView>
  );
};
export default ProfileScreen;
