import React, { Component, useState } from "react";
import { ToastAndroid, ActivityIndicator, View } from "react-native";
import TransactionList from "../TransactionList";
// import { getTransaction } from "../../../API";
import {
  AUTH_FAILED,
  SUCCESS_GET_TRANSACTION,
  FAILED_NETWORK,
  NULL_TRANSACTION,
  NETWORK_ERROR,
  INTERNET_ERROR_MSG,
  NULL_ERROR_MSG,
} from "../../../../Values/Strings";
import { Indicator } from "../../../../Components/Indicator";

const TransactionContainer = () => {
  const [state, setState] = useState({
    transactions: [],
    isNullTransaction: true,
    isLoading: true,
  });
  console.log("TransactionContainer" + state.isLoading);
  if (state.isLoading) return <Indicator />;
  else {
    if (state.isNullTransaction) return <View />;
    return <TransactionList transactions={state.transactions} />;
  }
};
