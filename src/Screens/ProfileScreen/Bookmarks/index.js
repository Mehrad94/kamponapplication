import React, { useEffect, useState } from "react";
import { View, Text, FlatList, TouchableOpacity, StyleSheet, Image } from "react-native";
import Styles from "./styles";
import { useContext } from "react";
import { Context as UserContxt } from "../../../Context/userDataContext";
// import { getBookmark } from "../../Api/getBookMark";
import AsyncStorage from "@react-native-community/async-storage";
import { mV8, Flex, centerAll, iranSans } from "../../../Values/Theme";
import Tabs from "./BookMarkList/BookTabContent";
import { getItemById } from "../../../API/getItemById";
import SearchRow from "../../Rows/SearchRow";

const Bookmarks = ({ navigation }) => {
  //=================== CONTEXT ======================
  const context = useContext(UserContxt);
  const { state } = context;
  const { userInfo } = state;
  const clubBook = userInfo.clubBookmark;
  const disBook = userInfo.discountBookmark;
  //=================== CONTEXT ======================
  const [disBookArray, setDisBookArray] = useState([]);
  const [clubBookArray, setClubBookArray] = useState([]);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState(disBookArray);
  console.log({ state });
  useEffect(() => {
    setData(disBookArray);
  }, [disBookArray]);
  useEffect(() => {
    const type = "club";
    bookMarkManager(clubBook, type);
  }, [clubBook]);
  console.log({ disBook });

  useEffect(() => {
    const type = "dis";
    bookMarkManager(disBook, type);
  }, [disBook]);
  const bookMarkManager = (array, type) => {
    const bookLoop = array.map(async (item) => {
      const resIsExist = await checkInAsyncStorage(item);
      console.log({ resIsExist });

      if (resIsExist === "{}" || !resIsExist) {
        console.log("hh");

        const bookRes = await getItemById(type, item);
        console.log({ bookRes });
        if (typeof bookRes === "object") {
          let object = { ...bookRes };
          AsyncStorage.setItem(`${item}`, JSON.stringify(object));
        }
      }
      const finalRes = await checkInAsyncStorage(item);
      return JSON.parse(finalRes);
    });
    Promise.all(bookLoop).then((res) => {
      console.log({ res });
      if (res.length > 0) {
        console.log("ok");
        if (type === "dis") {
          setDisBookArray(res);
        } else if (type === "club") {
          setClubBookArray(res);
        }
      } else {
        setData(null);
        setClubBookArray([]);
        setDisBookArray([]);
      }
      setLoading(false);
    });
  };
  const checkInAsyncStorage = async (item) => {
    console.log("check");
    console.log({ ITEM: item });

    const check = await AsyncStorage.getItem(item.toString());
    console.log({ check });

    return check;
  };
  //=================== CONTEXT ======================
  const title = "علاقه مندی ها";
  // if (loading) {
  //   return <Indicator />;
  // }
  console.log({ data });

  const renderlist = () => {
    if (data) {
      if (data.length < 1) {
        return (
          <View style={[Flex, centerAll]}>
            <Text style={[iranSans]}>اطلاعاتی یافت نشد</Text>
          </View>
        );
      }
    } else {
      return (
        <View style={[Flex, centerAll]}>
          <Text style={[iranSans]}>اطلاعاتی یافت نشد</Text>
        </View>
      );
    }

    return (
      <FlatList
        initialNumToRender={4}
        keyExtractor={(CategoryList) => CategoryList.list}
        data={data}
        renderItem={({ item }) => {
          console.log({ item });

          return <SearchRow searchItem={item} />;
        }}
      />
    );
  };

  const _onTabClick = (isActive) => {
    {
      isActive ? setData(clubBookArray) : setData(disBookArray);
    }
  };

  return (
    <View style={[Flex, mV8]}>
      <Tabs onTabClick={_onTabClick} />
      {renderlist()}
    </View>
  );
};

export default Bookmarks;
// useEffect(() => {
//   navigation.addListener("didBlur", (path) => {
//     if (path.type === "didBlur") {
//     }
//   });
//   navigation.addListener("didFocus", (path) => {
//     if (path.type === "didFocus") {
//       console.log({ mori: state });

//       setLoading(true);
//       bookMarkManager();
//     }
//   });
// }, [navigation]);
