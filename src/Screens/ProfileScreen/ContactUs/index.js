import React, {useState, useEffect} from 'react';
import {View, Text, Linking, TouchableOpacity, Image} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Icon1 from 'react-native-vector-icons/FontAwesome5';
import Styles from './styles';
import logo from '../../../Assets/Png/drclubs.png'

const ContactUs = ({navigation}) => {

  const _onWebsiteClicked = () => {
    Linking.openURL("https://www.drclubs.ir/");
  };

  const _onInstagramClicked = () => {
    Linking.openURL("https://instagram.com/drclubs.ir");
  };
  
  const _onTelegramClicked = () => {
    Linking.openURL("https://t.me/customer_club");
  };
  const _onLinkdinClicked = () => {
    Linking.openURL("https://www.linkedin.com/in/drclubs");
  };
  const Category = {
  
    site: 'www.drclubs.ir',
    mobile: '021_91300161',
    instagram: 'drclubs.ir',
    telegram: 'customer_club',
    linkdin: 'drclubs'
  };
  return (
    <View style={Styles.Container}>

      <View style={Styles.TopBack}>
        <Image source={logo} style ={Styles.img}/>
      </View>
      <View style={Styles.ViewCenter}>

          <TouchableOpacity  style={Styles.Number} onPress={_onWebsiteClicked}>
            <Icon name="earth" size={30} color =  "#aaa" />
            <Text style={Styles.TextInput}>{Category.site}</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={_onInstagramClicked} style={Styles.Number}>
            <Icon name="instagram" size={30} color= "#aaa" />
            <Text style={Styles.TextInput}>{Category.instagram}</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={_onTelegramClicked } style={Styles.Number}>
            <Icon1 name="telegram-plane" size={30} color= "#aaa" />
            <Text style={Styles.TextInput}>{Category.telegram}</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={_onLinkdinClicked } style={Styles.Number}>
            <Icon name="linkedin-square" size={30} color= "#aaa" />
            <Text style={Styles.TextInput}>{Category.linkdin}</Text>
          </TouchableOpacity>

          <View style={Styles.Number}>
            <Icon name="phone" size={30} color= "#aaa" />
            <Text style={Styles.TextInput}>{Category.mobile}</Text>
          </View>

       
      </View>
    </View>
  );
};

export default ContactUs;
