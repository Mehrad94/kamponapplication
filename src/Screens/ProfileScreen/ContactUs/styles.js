import {StyleSheet, Dimensions} from 'react-native';
const { width} = Dimensions.get('screen');
import {mainColor} from '../../../Common/Colors';

const Styles = StyleSheet.create({
  Container: {
    flex: 1,
  },
  TopBack: {
    height: width/1.2,
    backgroundColor: mainColor,
    borderBottomStartRadius: 50,
    borderBottomEndRadius: 50,
  },
  Text: {
    fontFamily: 'iran_sans',
    fontSize: 35,
    paddingVertical: 40,
    paddingHorizontal: 25,
  },
  Edit: {
    position: 'absolute',
    right: 10,
    top: 10,
  },
  ViewCenter: {
    position: 'absolute',
    width: width/1.1,
    height: width,
    backgroundColor: '#e8e8e8',
    borderRadius: 25,
    alignSelf: 'center',
    top: width / 2.5,
  },


  Number: {
    flexDirection: 'row',
    paddingVertical: 20,
    paddingHorizontal: 15,
  },
  TextInput: {
    fontFamily: 'iran_sans',
    marginLeft: 10,
    marginTop: 5,
  },
  img: {
    height:width/3,
    alignSelf: 'center',
    marginVertical:10
  }
});
export default Styles;
