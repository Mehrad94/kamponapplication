import React from "react";
import { FlatList, Text, View } from "react-native";
// import { KamponRow } from "../KamponActiveRow";
import { centerAll, Flex, h3, iranSans } from "../../../../../Values/Theme";
import KamponRow from "../ContentRow";

export const ContentList = ({ data, navigate }) => {
  console.log({ data });

  const renderItems = (kampon) => {
    return <KamponRow navigate={navigate} searchItem={kampon.discount} />;
  };
  if (data != undefined) {
    if (data.length > 0) {
      return (
        <View style={[Flex]}>
          <FlatList
            data={data}
            renderItem={({ item }) => renderItems(item)}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      );
    } else {
      return (
        <View style={[centerAll, Flex]}>
          <Text style={[iranSans, h3]}>اطلاعاتی یافت نشد</Text>
        </View>
      );
    }
  } else return <View />;
};
