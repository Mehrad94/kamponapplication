import React, { Component } from "react";
import { BackHandler, Text, View } from "react-native";
import { BOUGHT_KAMPONS } from "../../../Values/Strings";
import { Flex, h2, iranSans, mV16, Tac } from "../../../Values/Theme";
import KamponTabScreen from "./KamponTabScreen";

const KamponScreen = () => {
  return (
    <View style={[Flex]}>
      <Text style={[iranSans, h2, Tac, mV16]}>{BOUGHT_KAMPONS}</Text>
      <KamponTabScreen />
    </View>
  );
};
export default KamponScreen;
