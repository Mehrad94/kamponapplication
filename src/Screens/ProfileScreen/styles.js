import { Dimensions, StyleSheet } from "react-native";
import { bgWhite, r100, centerAll, iranSans, h3, fWhite, r8, h4 } from "../../Values/Theme";
import { screenWidth } from "../../Values/Constants";
import { MAIN_COLOR, DEACTIVATE_COLOR } from "../../Values/Colors";

const { height, width } = Dimensions.get("screen");
// const heightTopContainer = height / 2.5;
// const heightProfileNumber = height / 8;
// const marginProfileNumber = width / 20;
export default styles = StyleSheet.create({
  container: {
    flex: 1,
    ...centerAll
  },
  topView: {
    ...centerAll,
    height: height / 3,
    backgroundColor: MAIN_COLOR,
    width: "100%",
    borderBottomLeftRadius: 16,
    borderBottomRightRadius: 16
  },
  mainView: {
    width: "100%",
    padding: 16,
    backgroundColor: "#eee"
  },
  rows: {
    width: "100%",
    flexDirection: "row-reverse",
    elevation: 1,
    backgroundColor: "white",
    padding: 16,
    marginBottom: 4
  },
  text: {
    ...iranSans,
    ...h4,
    marginRight: 24,
    color: "#aaa"
  },
  image: {
    borderWidth: 2,
    borderColor: "white",
    backgroundColor: "transparent",
    width: width / 4,
    height: width / 4,
    borderRadius: 100,
    marginBottom: 8
  },
  firsRows: {
    width: "100%",
    flexDirection: "row-reverse",
    elevation: 1,
    backgroundColor: "white",
    padding: 16,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    marginBottom: 4
  },
  lastRows: {
    width: "100%",
    flexDirection: "row-reverse",
    elevation: 1,
    backgroundColor: "white",
    padding: 16,
    borderBottomLeftRadius: 16,
    borderBottomRightRadius: 16,
    marginBottom: 8
  }
});
