import React, { Component, useState, useEffect, useContext } from "react";
import { View, Text, StyleSheet, StatusBar, Button } from "react-native";
import ConfettiCannon from "react-native-confetti-cannon";
import WheelOfFortune from "./NewFortune";
import { MAIN_COLOR, DEACTIVATE_COLOR } from "../../Values/Colors";
import { getScenarioSpinner } from "../../API/getScenarioSpinner";
import { posAbs, centerAll, fWhite, h3, iranSans } from "../../Values/Theme";
import { Indicator } from "../../Components/Indicator";
import { Context as DataContext } from "../../Context/DataContext";
const rewardString = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"];
const participants = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const colors = [MAIN_COLOR, DEACTIVATE_COLOR];
const FurtuneWheel = () => {
  const context = useContext(DataContext);
  const { spinnerActions } = context;
  const { giftArray, timeSpin, gift, maxSpin } = context.state;
  console.log({ giftArray, timeSpin, gift, maxSpin });

  const [state, setState] = useState({
    winnerValue: null,
    winnerIndex: null,
  });
  const [explosion, setExplosion] = useState(false);
  const [loading, setloading] = useState(false);
  let child = null;
  useEffect(() => {
    if (state.winnerIndex) {
      setExplosion(true);
    }
  }, [state.winnerIndex]);
  useEffect(() => {
    spinnerActions();
  }, []);
  const fetchScenarioSpinner = async () => {
    console.log("morteza");

    setGiftArray(giftArray);
    setTimeSpin(resScenario.canSpin);
  };

  const scenarioProcess = () => {
    const TEXT = "در حال حاظر مسابقه ای وجود ندارد";
    return (
      <View
        style={[
          posAbs,
          centerAll,
          { width: "100%", height: "100%", backgroundColor: "rgba(52, 52, 52, 0.8)", zIndex: 2000 },
        ]}
      >
        <Text style={[fWhite, h3, iranSans]}>{TEXT}</Text>
      </View>
    );
  };
  const calTimeSpin = () => {
    let time = null;
    if (typeof timeSpin === "boolean") {
      time = timeSpin;
    } else if (typeof timeSpin === "number") {
      time = timeSpin / 1000;
    } else {
      time = timeSpin;
    }
    return time;
  };
  if (loading || giftArray.length < 1) {
    return <Indicator />;
  }
  const onreload = () => {
    setloading(true);
    // setTimeout(() => {
    spinnerActions();
    setloading(false);
    //   setloading(false);
    // }, 1000);
  };

  return (
    <View style={styles.container}>
      <StatusBar barStyle={"light-content"} />
      <WheelOfFortune
        reload={onreload}
        maxSpin={maxSpin / 1000}
        gift={gift}
        giftIndex={giftArray.findIndex((el) => el === gift)}
        refreshData={spinnerActions}
        timeSpin={calTimeSpin()}
        colors={colors}
        onRef={(ref) => (child = ref)}
        rewards={giftArray}
        knobSize={32}
        borderWidth={3}
        borderColor={"#FFF"}
        innerRadius={10}
        duration={5000}
        backgroundColor={"#c0392b"}
        winner={5}
        getWinner={(value, index) => setState({ winnerValue: value, winnerIndex: index })}
      />
      {/* {scenarioProcess()} */}
      {explosion ? <ConfettiCannon count={150} origin={{ x: -10, y: 0 }} /> : null}
    </View>
  );
};
export default FurtuneWheel;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    // backgroundColor: "#E74C3C"
  },
  winner: {
    width: "100%",
    position: "absolute",
    padding: 10,
    backgroundColor: "#fff",
    bottom: 50,
    justifyContent: "center",
    alignItems: "center",
  },
  winnerText: {
    fontSize: 26,
    color: "#666",
  },
  tapToStart: {
    fontSize: 50,
    color: "#fff",
    fontWeight: "bold",
  },
});
