import React, { useState, useEffect } from "react";
import {
  Animated,
  Dimensions,
  Text as RnText,
  Image as RNImage,
  TouchableOpacity,
  View,
} from "react-native";
import styles from "./style";
import * as d3Shape from "d3-shape";
import Svg, { G, Path, Text, TSpan } from "react-native-svg";
import { DEACTIVATE_COLOR, MAIN_COLOR } from "../../Values/Colors";
import NOOBIMAGE from "../../Assets/fortune/images/knoob.png";
import CountDown from "react-native-countdown-component";
import { Dialog } from "react-native-simple-dialogs";
import {
  iranSans,
  h4,
  Tac,
  centerAll,
  fMainColor,
  h6,
  h5,
  fWhite,
  posAbs,
  h3,
} from "../../Values/Theme";
import { SPIN, YOUR_REWARD, NEXT_SPIN } from "../../Common/Strings";
import Sound from "react-native-sound";
import Music from "../../Assets/spin.mp3";
import { postSpinner } from "../../API/postSpiner";
import { Indicator } from "../../Components/Indicator";
const FortuneWheel = (props) => {
  console.log({ props });
  //===================================STATE ======================
  const [angle, setAngel] = useState(new Animated.Value(0));
  const [state, setState] = useState({
    enabled: false,
    started: false,
    finished: false,
    winner: null,
    gameScreen: new Animated.Value(width - 40),
    wheelOpacity: new Animated.Value(1),
    imageLeft: new Animated.Value(width / 2 - 30),
    imageTop: new Animated.Value(height / 2 - 70),
  });
  const [modal, setModal] = useState(false);
  const [startSpin, setStartSpin] = useState(false);
  const [disAbleSpin, setDisableSpin] = useState(false);
  const [wheelPath, setWeelPath] = useState(null);
  const [spinCount, setSpinCount] = useState(0);
  //===================================STATE ======================

  useEffect(() => {
    if (spinCount > 1) {
      angle.setValue(0);
    }
  }, [startSpin]);
  //================================CONST=========================
  const { width, height } = Dimensions.get("screen");
  const wheelSize = width * 0.9;
  const fontSize = 26;
  const Rewards = props.rewards;
  const RewardCount = Rewards.length;
  const numberOfSegments = props.rewards.length;
  const colors = [
    "#381460",
    "#fe346e",
    "#dd2c00",
    "#ff5722",
    "#413c69",
    "#400082",
    "#00bdaa",
    "#ffd868",
    "#42240c",
    "#363636",
  ];
  const oneTurn = 360;
  const angleBySegment = oneTurn / numberOfSegments;
  const angleOffset = angleBySegment / 2;
  const winner = props.winner;
  let ANGLE = 0;

  useEffect(() => {
    angle.addListener((event) => {
      if (state.enabled) {
        setState({ ...state, enabled: false, finished: false });
      }
      ANGLE = event.value;
    });
  }, []);

  //================================CONST=========================

  //================================ SOUND =========================
  let song = null;
  useEffect(() => {
    song = new Sound("spin_main.mp3", Sound.MAIN_BUNDLE, (error) => {
      console.log({ error });

      if (error) {
        console.log("failed to load the sound", error);
        return;
      }
    });
  }, []);
  const onPressSong = () => {
    song.play((sucsses) => {
      if (sucsses) {
        console.log("ok");
      } else {
        console.log("nok");
      }
    });
  };

  //================================ SOUND =========================

  //============================== MAKE WHEELL AND COLORS ==============================
  const makeWheel = () => {
    const data = Array.from({ length: numberOfSegments }).fill(1);
    const arcs = d3Shape.pie()(data);
    const sort = arcs.sort((a, b) => {
      return a.index - b.index;
    });
    return sort.map((arc, index) => {
      const instance = d3Shape
        .arc()
        .padAngle(0.01)
        .outerRadius(width / 2)
        .innerRadius(20);

      return {
        path: instance(arc),
        color: colors[index],
        value: Rewards[index],
        centroid: instance.centroid(arc),
      };
    });
  };
  //============================== MAKE WHEELL AND COLORS ==============================

  useEffect(() => {
    console.log({ JJ: props.rewards });

    let wheelPathsI = makeWheel();
    setWeelPath(wheelPathsI);
  }, [startSpin]);
  //========================== RANDOM WINNER ================================
  const getwinnerIndex = () => {
    const deg = Math.abs(Math.round(ANGLE % oneTurn));

    if (ANGLE < 0) {
      return Math.floor(deg / angleBySegment);
    }
    return (numberOfSegments - Math.floor(deg / angleBySegment)) % numberOfSegments;
  };
  //========================== RANDOM WINNER ================================
  const postGift = async () => {
    const respostSpin = await postSpinner(props.gift);
    console.log({ respostSpin });
  };
  const onPan = async () => {
    setDisableSpin(true);
    setStartSpin(false);
    // onPressSong();
    postGift();
    console.log({ onPress: props.giftIndex });

    Animated.timing(angle, {
      toValue: 360 - props.giftIndex * (oneTurn / numberOfSegments) + 360 * (10000 / 1000),
      duration: 5000,
      useNativeDriver: true,
    }).start(() => {
      setDisableSpin(false);

      const winnerIndex = getwinnerIndex();
      setState({
        ...state,
        finished: true,
        winner: typeof props.gift === "string" ? props.gift : "poch",
        started: false,
      });
      props.getWinner(wheelPath[winnerIndex].value, winnerIndex);
      setModal(true);
      props.refreshData();
    });

    // _wheelPaths[winnerIndex].value  WINNER
  };

  //=================================RENDER NOOB ======================
  const _renderKnob = () => {
    const knobSize = 35;
    const YOLO = Animated.modulo(
      Animated.divide(
        Animated.modulo(Animated.subtract(angle, angleOffset), oneTurn),
        new Animated.Value(angleBySegment)
      ),
      1
    );
    return (
      <Animated.View
        style={{
          alignSelf: "center",
          width: knobSize,
          height: knobSize * 2,
          zIndex: 1,
          opacity: state.wheelOpacity,
          transform: [
            {
              rotate: YOLO.interpolate({
                inputRange: [-1, -0.5, -0.0001, 0.0001, 0.5, 1],
                outputRange: ["0deg", "0deg", "35deg", "-35deg", "0deg", "0deg"],
              }),
            },
          ],
        }}
      >
        <Svg
          width={knobSize}
          height={(knobSize * 100) / 57}
          viewBox={`0 0 57 100`}
          style={{ transform: [{ translateY: 8 }] }}
        >
          <RNImage source={NOOBIMAGE} style={{ width: knobSize, height: (knobSize * 100) / 35 }} />
        </Svg>
      </Animated.View>
    );
  };
  //=================================RENDER NOOB ======================

  //=================================RENDER SVGWHEEL ======================
  const renderSvgWheel = () => {
    return (
      <View style={{ flex: 1, alignItems: "center" }}>
        {_renderKnob()}

        <Animated.View
          style={{
            height: height / 2.02,
            alignItems: "center",
            justifyContent: "center",
            transform: [
              {
                rotate: angle.interpolate({
                  inputRange: [-oneTurn, 0, oneTurn],
                  outputRange: [`-${oneTurn}deg`, `0deg`, `${oneTurn}deg`],
                }),
              },
            ],
          }}
        >
          <View>
            <Svg
              width={wheelSize}
              height={wheelSize}
              viewBox={`0 0 ${width} ${width}`}
              style={{ transform: [{ rotate: `-${angleOffset}deg` }] }}
            >
              <G y={width / 2} x={width / 2}>
                {wheelPath.map((arc, index) => {
                  const [x, y] = arc.centroid;
                  const number = arc.value;
                  return (
                    <G key={`arc-${index}`}>
                      <Path d={arc.path} strokeWidth={2} fill={arc.color} />
                      <G
                        rotation={(index * oneTurn) / numberOfSegments + angleOffset}
                        origin={`${x}, ${y}`}
                      >
                        <Text
                          x={x}
                          y={y - 60}
                          fill={"white"}
                          textAnchor={"middle"}
                          fontSize={16}
                          fontFamily="iran_sans"
                        >
                          {Array.from({ length: number.length }).map((_, j) => {
                            return (
                              <TSpan x={x} dy={14} key={`slice-${j}`}>
                                {number.charAt(j)}
                              </TSpan>
                            );
                          })}
                        </Text>
                      </G>
                    </G>
                  );
                })}
              </G>
            </Svg>
          </View>
        </Animated.View>
      </View>
    );
  };
  //=================================RENDER SVGWHEEL ======================

  //=================================COUNT DOWN  ======================
  console.log({ SSS: props.timeSpin, startSpin });

  useEffect(() => {
    if (typeof props.timeSpin === "boolean") {
      if (props.timeSpin === true) {
        setStartSpin(true);
      }
    }
  }, [props.timeSpin]);
  const startSpinActions = (type) => {
    if (type === "type2") {
      setStartSpin(true);
      setSpinCount(spinCount + 1);
    }
    if (type === "type1") {
      setStartSpin(true);
      props.reload();
    }
  };
  console.log({ LLL: props.gift, LL: props.giftIndex });

  const countDown = () => {
    if (typeof props.timeSpin === "number") {
      return (
        <View style={[centerAll]}>
          <RnText style={[iranSans, h6, fWhite]}>{NEXT_SPIN}</RnText>
          <CountDown
            until={props.timeSpin + 20}
            size={15}
            onFinish={() => startSpinActions("type1")}
            digitStyle={{
              backgroundColor: "#FFF",

              borderWidth: 2,
              borderColor: DEACTIVATE_COLOR,
            }}
            digitTxtStyle={[fMainColor, h5, iranSans]}
            timeToShow={["D", "H", "M", "S"]}
            timeLabels={{ h: null, m: null, s: null }}
          />
        </View>
      );
    } else {
      return (
        <View style={[centerAll]}>
          <RnText style={[iranSans, h6, fWhite]}>{NEXT_SPIN}</RnText>
          <CountDown
            until={props.maxSpin + 20}
            size={15}
            onFinish={() => tartSpinActions("type2")}
            digitStyle={{
              backgroundColor: "#FFF",

              borderWidth: 2,
              borderColor: DEACTIVATE_COLOR,
            }}
            digitTxtStyle={[fMainColor, h5, iranSans]}
            timeToShow={["D", "H", "M", "S"]}
            timeLabels={{ h: null, m: null, s: null }}
          />
        </View>
      );
    }
  };
  //=================================COUNT DOWN  ======================

  const customDialogue = () => {
    if (modal) {
      return (
        <View style={[posAbs, { bottom: 150 }]}>
          <RnText style={[iranSans, h3, Tac]}>{` ${YOUR_REWARD} \n ${state.winner}`}</RnText>
        </View>
      );
    } else {
      return null;
    }
  };

  const renderTimer = () => {
    return (
      <TouchableOpacity
        disabled={!startSpin || disAbleSpin}
        style={styles.btnStartWheel}
        onPress={onPan}
      >
        {startSpin ? <RnText style={styles.txtStartWheel}>{SPIN}</RnText> : countDown()}
      </TouchableOpacity>
    );
  };
  return (
    <View style={styles.container}>
      {renderTimer()}
      {wheelPath ? renderSvgWheel() : <Indicator />}
      {customDialogue()}
    </View>
  );
};
export default FortuneWheel;
// <Dialog
//   dialogStyle={{
//     borderRadius: 25,
//     borderColor: MAIN_COLOR,
//     borderWidth: 2,
//     paddingVertical: 24,
//   }}
//   animationType={"fade"}
//   titleStyle={[iranSans, h4, Tac]}
//   visible={modal}
//   title={""}
//   onTouchOutside={() => {
//     setModal(false);
//   }}
// >
//   <View style={[centerAll]}>
//     <RnText>
//       {YOUR_REWARD}:{state.winner}
//     </RnText>
//   </View>
// </Dialog>
