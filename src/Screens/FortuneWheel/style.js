import { Dimensions, StyleSheet } from "react-native";
import { centerAll, hwHalf, iranSans } from "../../Values/Theme";
import { DEACTIVATE_COLOR, MAIN_COLOR } from "../../Values/Colors";

const { width, height } = Dimensions.get("screen");

const styles = StyleSheet.create({
  container: { flex: 1, ...centerAll },

  btnStartWheel: {
    position: "absolute",
    bottom: 50,
    borderRadius: 8,
    backgroundColor: MAIN_COLOR,
    paddingHorizontal: 100,
    // paddingVertical: 16,
    height: height / 10,
    color: "white",
    zIndex: 1000,
    alignItems: "center",
    justifyContent: "center",
  },
  txtStartWheel: {
    ...iranSans,

    color: "white",
    fontSize: 20,
  },
});

export default styles;
