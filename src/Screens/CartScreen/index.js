import React, { useEffect, useState, useContext } from "react";
import { View } from "react-native";
import StepIconContainer from "./StepIconContainer";
import { Context as CartContext } from "../../Context/CartScreenStepsContext";
const CartScreen = (props) => {
  const context = useContext(CartContext);
  const { state, setCartItems } = context;
  const [focus, setFocus] = useState(false);
  const [State, setState] = useState({ Ticket: false, MemberId: null });
  // console.log({ state });

  useEffect(() => {
    const ticket = props.navigation.getParam("ticket", "NO-ID");
    setState({ Ticket: ticket });
    setCartItems();
  }, []);
  // useEffect(() => {
  //   props.navigation.addListener("didFocus", () => {
  //     setFocus(true);
  //   });
  //   props.navigation.addListener("didBlur", () => {
  //     setFocus(false);
  //   });
  // }, [props]);

  // useEffect(() => {
  //   if (focus) {
  //     setCartItems();
  //   }
  // }, [focus]);

  return (
    <View style={{ flex: 1 }}>
      <StepIconContainer
        focus={focus}
        navigation={props.navigation}
        ticket={State.Ticket}
        memberId={State.MemberId}
      />
    </View>
  );
};
export default CartScreen;
