import React, { Component } from "react";
import { View, Text } from "react-native";
import Ticket from "../Ticket";
import CartItemListContainer from "../../../Containers/CartItemListContainer";
import Payment from "../../Payment";
const ContentStep = (props) => {
  switch (props.step) {
    case 0:
      return <CartItemListContainer navigation={props.navigation} />;
    case 1:
      return <Payment />;
    case 2:
      return <Ticket />;
    default:
      return (
        <View>
          <Text>lkjk</Text>
        </View>
      );
  }
};
export default ContentStep;
