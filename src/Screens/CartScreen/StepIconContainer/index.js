import React, { useEffect, useState, useContext } from "react";
import { Linking, View } from "react-native";
import { bgGray, Flex } from "../../../Values/Theme";
import ButtonStep from "./ButtonStep";
import CircleStep from "./CircleStep";
import ContentStep from "./ContetntStep";
import { Context as StepContext } from "../../../Context/CartScreenStepsContext";
import { getItemsInCart } from "../../../API/getItemsInCart";

const StepIconContainer = ({ navigation, ticket, memberId, focus }) => {
  const context = useContext(StepContext);
  const { onStepChange } = context;
  const { step } = context.state;
  // console.log(context.state);
  useEffect(() => {
    if (ticket === true) onStepChange(2);
  }, [ticket]);

  return (
    <View style={[Flex, bgGray]}>
      <CircleStep step={step} />

      <ContentStep step={step} navigation={navigation} memberId={memberId} />

      <View style={{ position: "absolute", bottom: 0, left: 0, right: 0 }}>
        <ButtonStep />
      </View>
    </View>
  );
};
export default StepIconContainer;
