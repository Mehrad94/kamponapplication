import React, { Component } from "react";
import { View, Text } from "react-native";
import { bgGray, iranSans } from "../../../../Values/Theme";
import styles from "./styles";
const CircleStep = (props) => {
  return (
    <View style={styles.Container}>
      <View style={props.step === 0 ? styles.activeCircle : styles.inActiveCircle}>
        <Text style={props.step === 0 ? [{ color: "white", fontSize: 16 }, iranSans] : [iranSans, { color: "black", fontSize: 16 }]}>1</Text>
      </View>
      <View style={styles.activeBar} />
      <View style={props.step === 1 ? styles.activeCircle : styles.inActiveCircle}>
        <Text style={props.step === 1 ? [{ color: "white", fontSize: 16 }, iranSans] : [{ color: "black", fontSize: 16 }, iranSans]}>2</Text>
      </View>
      <View style={styles.activeBar} />
      <View style={props.step === 2 ? styles.activeCircle : styles.inActiveCircle}>
        <Text style={props.step === 2 ? [{ color: "white", fontSize: 16 }, iranSans] : [{ color: "black", fontSize: 16 }, iranSans]}>3</Text>
      </View>
    </View>
  );
};
export default CircleStep;
