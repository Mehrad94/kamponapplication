import { StyleSheet } from "react-native";
import { MAIN_COLOR } from "../../../../Values/Colors";
import { bgGray } from "../../../../Values/Theme";

const styles = StyleSheet.create({
  Container: {
    flexDirection: "row-reverse",
    alignItems: "center",
    justifyContent: "center",
    margin: 16,
    ...bgGray
  },
  activeCircle: {
    width: 48,
    height: 48,
    borderRadius: 100,
    alignItems: "center",
    justifyContent: "center",

    backgroundColor: MAIN_COLOR
  },
  inActiveCircle: {
    width: 48,
    height: 48,
    borderRadius: 100,
    alignItems: "center",
    borderColor: MAIN_COLOR,
    borderWidth: 5,
    justifyContent: "center",
    backgroundColor: "white"
  },
  activeBar: {
    width: 70,
    backgroundColor: MAIN_COLOR,
    height: 10
  }
});
export default styles;
