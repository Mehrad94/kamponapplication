import React, { useContext } from "react";
import { View, Text, Linking, TouchableOpacity } from "react-native";
import styles from "./styles";
import { activeButton, mV8, iranSans, h4, fWhite } from "../../../../Values/Theme";
import { screenWidth } from "../../../../Values/Constants";
import { Context as StepContext } from "../../../../Context/CartScreenStepsContext";
import { GetBankUrl } from "../../../../API";

const ButtonStep = (props) => {
  const context = useContext(StepContext);
  const { onStepChange } = context;
  const { step, buttonActive } = context.state;
  const btnPaymentClick = () => {
    console.log("j");
    onStepChange(1);
  };

  const btnBackToCart = () => {
    onStepChange(0);
  };

  const btnGotoPayment = async () => {
    const url = await GetBankUrl();
    Linking.openURL(url);
  };

  const gotoHomePage = () => {
    onStepChange(0);
    navigation.navigate("Kampons");
  };

  switch (step) {
    case 0:
      if (props.action) {
        return (
          <TouchableOpacity
            disabled={buttonActive}
            style={[activeButton, mV8, { backgroundColor: "#e8e8e8", width: screenWidth / 1.5, alignSelf: "center", height: 40 }]}
            onPress={() => btnPaymentClick()}
          >
            <Text style={[iranSans, h4, fWhite]}>سبد خرید خالی است</Text>
          </TouchableOpacity>
        );
      } else {
        return (
          <TouchableOpacity
            // disabled={buttonActive}
            style={[activeButton, mV8, { width: screenWidth / 1.5, alignSelf: "center", height: 40 }]}
            onPress={btnPaymentClick}
          >
            <Text style={[iranSans, h4, fWhite]}>پرداخت</Text>
          </TouchableOpacity>
        );
      }
    case 1:
      return (
        <View style={styles.PaymentScreen}>
          <TouchableOpacity style={styles.btnPayment} onPress={btnGotoPayment}>
            <Text style={[styles.btnTitle, iranSans]}>ورود به درگاه</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.btnPayment} onPress={btnBackToCart}>
            <Text style={[styles.btnTitle, iranSans]}>بازگشت</Text>
          </TouchableOpacity>
        </View>
      );
    case 2:
      return (
        <TouchableOpacity style={styles.btnNext} onPress={gotoHomePage}>
          <Text style={[styles.btnTitle, iranSans, h4]}>مشاهده کمپُن های من</Text>
        </TouchableOpacity>
      );
  }
};
export default ButtonStep;
