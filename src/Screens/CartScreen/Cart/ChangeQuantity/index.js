import React, { useContext, useState, useEffect } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import CustomIcon from "react-native-vector-icons/FontAwesome";
import { fBlack, h3, iranSans } from "../../../../Values/Theme";
import { Context as StepsConetxt } from "../../../../Context/CartScreenStepsContext";
const ChangeQuantity = ({ id, price, count }) => {
  const context = useContext(StepsConetxt);
  const { addToCart, setCartItems } = context;
  const { cartItems } = context.state;
  const [idState, setId] = useState(id);
  console.log({ bb: cartItems });
  const [counter, setCounter] = useState(count);
  const onIncreaseItem = () => {
    setCounter(counter + 1);
  };
  useEffect(() => {
    addToCart(idState, counter, () => setCartItems());
  }, [counter]);
  const onDecreaseItem = () => {
    if (counter != 1) {
      setCounter(counter - 1);
    }
  };

  const hitSlop = { top: 20, right: 10, bottom: 20, left: 10 };

  return (
    <View style={[styles.container]}>
      <TouchableOpacity style={styles.btnUp} hitSlop={hitSlop} onPress={() => onIncreaseItem(id)}>
        <CustomIcon name="plus" size={15} color="#8BC34A" />
      </TouchableOpacity>
      <Text style={[h3, iranSans, fBlack]}>{counter}</Text>
      <TouchableOpacity style={styles.btnDown} hitSlop={hitSlop} onPress={() => onDecreaseItem(id)}>
        <CustomIcon name="minus" size={15} color="#f44336" />
      </TouchableOpacity>
    </View>
  );
};
export default ChangeQuantity;

const styles = StyleSheet.create({
  container: {
    width: 30,
    backgroundColor: "#f7f8fa",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#d4dce1",
    borderRadius: 15,
    alignSelf: "flex-end"
  },
  text: {
    fontSize: 18,
    color: "red"
  },
  btnUp: {
    height: 40,
    justifyContent: "center",
    alignItems: "center"
  },
  btnDown: {
    height: 40,
    justifyContent: "center",
    alignItems: "center"
  }
});
