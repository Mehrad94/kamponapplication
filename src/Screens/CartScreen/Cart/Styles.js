import { Dimensions, StyleSheet } from "react-native";
import { iranSans, h4, mH8, mV4 } from "../../../Values/Theme";

export default Styles = StyleSheet.create({
  carItemContainer: {
    flex: 1,
    paddingVertical: 4,
    paddingHorizontal: 8
  },
  CartImageParent: {
    flexDirection: "row-reverse",
    borderRadius: 8,
    padding: 10,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center"
  },
  CartImage: {
    width: Dimensions.get("screen").width / 4,
    height: Dimensions.get("screen").width / 4,
    borderRadius: 8
  },
  Text: {
    ...iranSans,
    ...mH8,
    ...mV4,
    textAlign: "right"
  }
});
