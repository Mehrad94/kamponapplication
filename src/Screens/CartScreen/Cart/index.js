import React, { useContext, useEffect, useState } from "react";
import { Image, Text, View, TouchableHighlight } from "react-native";
import styles from "./Styles";
import ChangeQuantity from "../Cart/ChangeQuantity";
import { fGray } from "../../../Values/Theme";
import Icon from "react-native-vector-icons/FontAwesome5";
import { Context as CartContext } from "../../../Context/CartScreenStepsContext";
import { Button } from "react-native-elements";
import { Indicator } from "../../../Components/Indicator";
const CartItem = (props) => {
  const context = useContext(CartContext);
  const { deletCart, setCartItems } = context;
  const { cartItems } = context.state;

  const { thumbnail, title, newPrice, _id, realPrice } = props.data.item.discount;
  console.log({ kk: props.data });
  const { count } = props.data.item;

  const [isDelLoading, setDelLoading] = useState(false);
  const deleteFromCart = (id) => {
    setDelLoading(true);
    deletCart(id, () => setCartItems());
  };
  useEffect(() => {
    setTimeout(() => {
      setDelLoading(false);
    }, 3000);
  }, [props.data]);
  return (
    <View style={styles.carItemContainer}>
      <View style={styles.CartImageParent}>
        <Image source={{ uri: thumbnail }} style={styles.CartImage} />
        <View style={{ flex: 1, justifyContent: "center" }}>
          <Text style={[styles.Text]}>{title}</Text>
          <Text style={[styles.Text]}>{newPrice} تومان</Text>
          <Text style={[styles.Text, fGray]}>{realPrice} تومان</Text>
        </View>
        <ChangeQuantity id={_id} price={newPrice} count={count} />
      </View>
      <TouchableHighlight
        style={{
          position: "absolute",
          bottom: 15,
          left: 80,
          width: 50,
          height: 50,
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        {isDelLoading ? (
          <Indicator size={15} count={3} />
        ) : (
          <Icon name="trash" color={"#e53935"} size={15} onPress={() => deleteFromCart(_id)} />
        )}
      </TouchableHighlight>
    </View>
  );
};
export default CartItem;
