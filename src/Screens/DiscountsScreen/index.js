import React, { useEffect, useState } from "react";
import {
  ImageBackground,
  ScrollView,
  Text,
  View,
  Image,
  SafeAreaView,
  Dimensions,
} from "react-native";
import { NEW_PRICE, TOMAN, OLD_PRICE, PHONES, ADDRESS } from "../../Values/Strings";
import {
  bgWhite,
  el1,
  fBlack,
  fGreen,
  fLineT,
  fRed,
  fRow,
  fWhite,
  h3,
  h4,
  h5,
  iranSans,
  m8,
  mH16,
  mV8,
  pad8,
  padH8,
  padV8,
  r8,
  spaceB,
  center,
  s24,
  h6,
} from "../../Values/Theme";
import KamponFooter from "./KamponFooter";
import KamponTab from "./DiscountsTab";
import { TouchableOpacity } from "react-native-gesture-handler";
import BackgroundCursor from "../../Components/BackgroundCursor/BackgroundCursor";
import styles from "./styles";
const irAmount = require("iramount");

const DiscountsScreen = ({ data }) => {
  const amount = new irAmount(4511551);
  const { height } = Dimensions.get("window");
  console.log({ DataDISCOUNT: data });
  //=============================== DISCOUNTS DATA SEPREATE ========================
  let TabData = null;
  const { features, termsOfUse } = data;
  TabData = { features, termsOfUse };
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
        <ScrollView
          alwaysBounceVertical
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ justifyContent: "center" }}
          style={{ marginBottom: 45, height: height }}
        >
          <BackgroundCursor images={data.slides} />
          <View style={styles.topContentHolder}>
            <Text style={[styles.topContentText, mH16]}>{data.title}</Text>
            <Text style={styles.topContentTextOwner}>{data.owner.title}</Text>
            <Text style={[iranSans, h5, mV8, mH16]}> {data.subTitle}</Text>
            <ImageBackground
              style={styles.imageBacground}
              source={require("../../Assets/Png/discount.png")}
            >
              <Text style={[iranSans, h6, fWhite]}>%{data.percent}</Text>
            </ImageBackground>
          </View>
          <View style={[bgWhite, el1, m8, padV8, padH8, r8]}>
            <View style={[fRow, spaceB]}>
              <Text style={[iranSans, h4, mV8, fGreen]}>
                {new irAmount(data.newPrice).digitGrouped()} {TOMAN}
              </Text>
              <Text style={[iranSans, h4, mV8, fGreen]}>{NEW_PRICE}</Text>
            </View>
            <View style={styles.spaeacer} />
            <View style={[fRow, spaceB]}>
              <Text style={[iranSans, h5, mV8, fRed, fLineT]}>
                {new irAmount(data.realPrice).digitGrouped()} {TOMAN}
              </Text>
              <Text style={[iranSans, h5, mV8, fRed]}>{OLD_PRICE}</Text>
            </View>
            <View style={styles.spaeacer} />
            <View style={[fRow, spaceB]}>
              <Text style={[iranSans, h5, mV8, fBlack]}>{data.owner.phone[0]}</Text>
              <Text style={[iranSans, h5, mV8, fBlack]}>{PHONES}</Text>
            </View>
            <View style={styles.spaeacer} />
            <View style={[fRow, { justifyContent: "flex-end" }]}>
              <Text style={styles.ownerAddresText}>{data.owner.address}</Text>
              <Text style={[iranSans, h5, mV8, fBlack]}> {ADDRESS} </Text>
            </View>
          </View>
          <TouchableOpacity style={[fRow, bgWhite, el1, m8, pad8, r8, center, spaceB]}>
            <Image style={[s24]} source={require("../../Assets/Png/back.png")} />
            <Text style={[iranSans, h5, mV8]}>گزارش اشکال</Text>
          </TouchableOpacity>
          <KamponTab tabContent={TabData} />
        </ScrollView>
        <KamponFooter data={data} />
      </View>
    </SafeAreaView>
  );
};

export default DiscountsScreen;
