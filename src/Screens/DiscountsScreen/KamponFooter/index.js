import React, { useEffect, useState, useContext } from "react";
// import { CheckDiscountInCart } from "../../../API/CheckDiscountInCart";
import DiscountShare from "../DiscountShare";
import { MAIN_COLOR } from "../../../Values/Colors";
import { TotalChanges } from "../../../API/TotalChanges";
import { Text, ToastAndroid, TouchableOpacity, View } from "react-native";
import { Dialog } from "react-native-simple-dialogs";
import { toggleBookmark } from "../../../API/toggleBookmark";
import { Indicator } from "../../../Components/Indicator";
import { AUTH_FAILED, BASE_URL, MSG_SMT_WRONG } from "../../../Values/Strings";
import {
  bgMainColor,
  bgRed300,
  center,
  centerAll,
  fRow,
  fWhite,
  h4,
  h5,
  iranSans,
  m8,
  pad16,
  pad8,
  padH8,
  posAbs,
  rMain,
  spaceA,
  Tac,
  w50,
} from "../../../Values/Theme";
import axios from "axios";
import CustomDialog from "../../Modal";
import Icon from "react-native-vector-icons/FontAwesome";
import { checkTokenInDevice } from "../../../API/checkTokenInDevice";
import { Context as CartContext } from "../../../Context/CartScreenStepsContext";
import { Context as UserContext } from "../../../Context/userDataContext";
const KamponFooter = ({ data, navigate }) => {
  const userContext = useContext(UserContext);
  const { getMemberInformation } = userContext;
  const userState = userContext.state;
  const context = useContext(CartContext);
  const { deletCart, addToCart, setCartItems } = context;
  const { cartItems, error } = context.state;
  // console.log({ DATA: data });

  const [isInCart, setIsInCart] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);
  const [showAlert, setShowAlert] = useState(false);
  const [loading, setLoading] = useState(true);
  const [mDesc, SetmDesc] = useState("");
  const [mTitle, setMTitle] = useState("");
  const [isInBookmarked, setIsInBookmarked] = useState(false);
  const [loadingBookmark, setLoadingBookmark] = useState(false);
  console.log({ isInCart });
  useEffect(() => {
    if (userState.userInfo) {
      if (userState.userInfo.discountBookmark.includes(data._id)) {
        setIsInBookmarked(true);
      } else {
        setIsInBookmarked(false);
      }
    }
  }, [userState.userInfo]);
  const checkExistInCart = () => {
    if (cartItems.length > 0) {
      const exist = cartItems.map((item) => {
        return item.discount._id;
      });
      if (exist.includes(data._id)) {
        setIsInCart(true);
        console.log({ IS: "jnkn" });
      } else {
        setIsInCart(false);
      }
    } else {
      setIsInCart(false);
    }
  };

  //===================== ADD AND RENOVE CART ===========================

  useEffect(() => {
    setBtnLoading(false);
  }, [cartItems]);

  useEffect(() => {
    setBtnLoading(false);
  }, [error]);
  useEffect(() => {
    checkExistInCart();
  }, [cartItems]);

  const handleRemoveFromCart = () => {
    setBtnLoading(true);
    setIsInCart(false);
    deletCart(data._id);
    setCartItems(() => checkExistInCart());
  };

  const handleAddToCart = async () => {
    setBtnLoading(true);
    setIsInCart(true);
    addToCart(data._id);
    setCartItems(() => checkExistInCart());
  };
  //===================== ADD AND RENOVE CART ===========================

  const _checkBookmarks = async () => {};

  //================================== ADD_BOOKMARK ====================================
  const _AddBookmark = async () => {
    setIsInBookmarked(!isInBookmarked);

    type = "DISCOUNT";
    const resBook = await toggleBookmark(data._id, type);
    console.log({ resBook });
    if (resBook === 2022) {
      getMemberInformation();
    }
    if (resBook === 2021) {
      getMemberInformation();
    }
  };
  //================================== ADD_BOOKMARK ====================================

  const _gotoLoginScreen = () => {
    setShowAlert(false);
    navigate("Login");
    setBtnLoading(false);
    setLoadingBookmark(false);
  };
  const onCancle = () => {
    setShowAlert(false);
    setBtnLoading(false);
    setLoadingBookmark(false);
  };
  const ButtonAction = () => {
    setShowAlert(false);
    setBtnLoading(false);
    setLoadingBookmark(false);
  };

  return (
    <View style={[fRow, posAbs, { bottom: 0, left: 0, right: 0 }]}>
      <TouchableOpacity
        onPress={isInCart ? handleRemoveFromCart : handleAddToCart}
        disabled={btnLoading}
        style={[
          centerAll,
          pad8,
          w50,
          isInCart ? { backgroundColor: "red" } : { backgroundColor: "#50C3C6" },
        ]}
      >
        {btnLoading ? (
          <Indicator size={28} count={3} color={"white"} />
        ) : (
          <Text style={[fWhite, iranSans, padH8, h4]}>
            {isInCart ? "حذف از سبد خرید" : "افزودن به سبد خرید"}
          </Text>
        )}
      </TouchableOpacity>
      <View style={[w50, fRow, spaceA, center, { backgroundColor: "#e8e8e8" }]}>
        <DiscountShare discountId={data._id} />

        {loadingBookmark ? (
          <Indicator size={20} count={3} color={MAIN_COLOR} />
        ) : (
          <Icon
            onPress={_AddBookmark}
            name={"bookmark"}
            color={isInBookmarked ? MAIN_COLOR : "gray"}
            size={25}
          />
        )}
      </View>
      <CustomDialog
        acceptButton={"تایید"}
        status={3}
        state={showAlert}
        title={mDesc}
        exButton={"انصراف"}
        accept={_gotoLoginScreen}
        oncancel={onCancle}
        ButtonAction={ButtonAction}
      />
    </View>
  );
};

export default KamponFooter;
