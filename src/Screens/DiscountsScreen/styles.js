import { StyleSheet, Dimensions } from "react-native";
import {
  bgAccentColor,
  bgWhite,
  iranSans,
  h2,
  s48,
  centerAll,
  posAbs,
  fBlack,
  mV8,
  h5,
  h4,
  h3,
} from "../../Values/Theme";

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    ...bgAccentColor,
  },
  topContentHolder: {
    ...bgWhite,
    elevation: 1,
    margin: 8,
    padding: 8,
    borderRadius: 8,
  },
  topContentText: {
    ...iranSans,
    ...h3,
  },
  topContentTextOwner: {
    ...iranSans,
    ...h5,
    color: "rgba(0, 0, 0, 0.5)",
    marginHorizontal: 24,
  },
  imageBacground: {
    ...s48,
    ...centerAll,
    ...posAbs,
    left: 0,
  },
  spaeacer: {
    height: 1,
    width: "100%",
    backgroundColor: "#e5e5e5",
  },
  ownerAddresText: {
    width: Dimensions.get("screen").width / 1.3,
    ...fBlack,
    ...mV8,
    ...h5,
    ...iranSans,
    textAlign: "right",
  },
});
