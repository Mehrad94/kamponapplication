import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet } from "react-native";
import DisandClubsList from "../DisCountLists/index.js";
import DisCountLists from "../DisCountLists/index.js";
import ClubsList from "../ClubsList/index.js";
import { iranSans, h4, fMainColor, h6, h5 } from "../../Values/Theme.js";
import { TEXT_COLOR } from "../../Common/Colors.js";
import { DIS_FILTER, CLUB_FILTER, TYPE } from "../../Common/Strings.js";
const DisAndClubsContainer = ({ data, most, title, filter }) => {
  const { clubs, discounts } = data;
  const [showDisCount, setShowDiscount] = useState(true);
  const [showClub, setShowClubs] = useState(true);

  useEffect(() => {
    if (filter === DIS_FILTER) {
      setShowClubs(false);
    } else {
      setShowClubs(true);
    }
    if (filter === CLUB_FILTER) {
      setShowDiscount(false);
    } else {
      setShowDiscount(true);
    }
    if (filter === TYPE) {
      setShowDiscount(true);
      setShowClubs(true);
    }
  }, [filter]);

  return (
    <View style={{ flex: 1, width: "100%" }}>
      <View style={styles.TextView}>
        <Text style={styles.Text}>{title}</Text>
        <Text style={styles.MoreText}>بیشتر</Text>
      </View>
      {showDisCount ? <DisCountLists data={most ? data : discounts} /> : null}
      {showClub ? most ? null : <ClubsList data={clubs} /> : null}
    </View>
  );
};
export default DisAndClubsContainer;
const styles = StyleSheet.create({
  Text: {
    ...iranSans,
    ...h5,
    marginRight: 16,
    color: TEXT_COLOR,
  },
  TextView: {
    flexDirection: "row-reverse",
    justifyContent: "space-between",
    alignItems: "center",
  },
  MoreText: {
    ...iranSans,
    ...h6,
    marginLeft: 16,
    color: TEXT_COLOR,
  },
});
