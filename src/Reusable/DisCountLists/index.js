import React, { useEffect } from "react";
import { View, Text, FlatList } from "react-native";
import DisRow from "../DisRow";
import DiscountRow from "../../Screens/Rows/DiscountRow";
const DisCountLists = ({ data }) => {
  const renderItem = (items) => {
    return <DiscountRow items={items} />;
  };
  return (
    <View style={{ flex: 1 }}>
      <FlatList
        showsHorizontalScrollIndicator={false}
        inverted
        horizontal
        data={data}
        renderItem={renderItem}
      />
    </View>
  );
};
export default DisCountLists;
