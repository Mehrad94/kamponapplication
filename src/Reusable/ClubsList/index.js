import React, { useEffect } from "react";
import { View, Text, FlatList } from "react-native";
import ClubsRow from "../ClubsRow";
const ClubsList = (props) => {
  const renderItem = (items) => {
    return <ClubsRow items={items} />;
  };
  return (
    <View>
      <FlatList
        showsHorizontalScrollIndicator={false}
        inverted
        horizontal
        data={props.data}
        renderItem={renderItem}
      />
    </View>
  );
};
export default ClubsList;
