import { StyleSheet, Dimensions } from "react-native";
import { screenWidth } from "../../Values/Constants";
import { centerAll, iranSans, h5, Tac } from "../../Values/Theme";

const style = StyleSheet.create({
  discountContainer: {
    marginTop: 8,
    marginBottom: 8,
    marginHorizontal: 4,
    width: screenWidth / 2,
    height: (screenWidth * 1.5) / 3.5,
    borderRadius: 5,
    backgroundColor: "white",
    elevation: 1,

    overflow: "hidden",
  },
  imgPlaceHolder: {
    width: "100%",
    height: screenWidth / 2,
  },
  discountImage: {
    width: "100%",
    height: screenWidth / 3,
  },

  discountTitleContainer: {
    textAlign: "center",
    marginLeft: 8,
    justifyContent: "center",
    fontFamily: "iran_sans",
  },

  discountPercentBg: {
    position: "absolute",
    width: 40,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    display: "flex",
    right: 0,
  },

  discountPercent: {
    color: "white",
    fontSize: 13,
    fontFamily: "iran_sans",
  },

  discountPriceContainer: {
    position: "absolute",
    bottom: 0,
    right: 0,
    marginRight: 10,
    alignItems: "center",
  },
  sellCountContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  discountSellCount: {
    fontSize: 13,
    marginLeft: 8,
    paddingTop: 2,
    textAlignVertical: "center",
  },
  discountPriceReal: {
    color: "red",
    textAlign: "left",
    marginLeft: 8,
    fontSize: 13,
    textDecorationLine: "line-through",
    fontFamily: "iran_sans",
  },
  discountPriceNew: {
    color: "green",
    fontSize: 18,
    fontFamily: "iran_sans",
  },
  discountName: {
    textAlign: "left",
    fontSize: 16,
    width: "90%",
    fontFamily: "iran_sans",
  },

  discountPrices: {
    flexDirection: "row",
    //justifyContent: "sp",
    alignItems: "center",
  },
  Content: {
    flexDirection: "row-reverse",
    justifyContent: "space-between",
  },
  RightContent: {
    alignItems: "center",
    marginRight: 6,
    justifyContent: "center",
  },
  Text: {
    ...iranSans,
    ...h5,
    ...Tac,
  },
  Icons: {
    // flexDirection: "row",
    // alignItems: "flex-start",
    // justifyContent: "space-between",
  },
  Icon: {
    flexDirection: "row",
    marginTop: 8,
    marginLeft: 8,
  },
  // Stars: {
  //   position: "absolute",
  //   top: 5,
  //   right: -10,
  // },
  RatingHolder: {
    backgroundColor: "red",
    justifyContent: "flex-start",
    alignItems: "flex-start",
  },
});

export default style;
