import React from "react";
import { Animated, Text, StyleSheet, TouchableWithoutFeedback, View } from "react-native";
import { bgWhite } from "../../Values/Theme";
import styles from "./styles";
import FastImage from "react-native-fast-image";
import { Rating } from "react-native-ratings";
import Icon from "react-native-vector-icons/FontAwesome5";
import { navigate } from "../../../navigationRef";

const ClubsRow = ({ items }) => {
  const { item } = items;

  const onPress = () => {
    navigate("ClubsScreen", { clubParams: item });
  };
  const Thumnail = { uri: item.slides[0] };

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.discountContainer}>
        <View style={[bgWhite]}>
          <FastImage style={styles.discountImage} source={Thumnail} />
          <View style={styles.Content}>
            <View style={styles.RightContent}>
              <Text style={styles.Text}> {item.title}</Text>
            </View>
            <View style={styles.Icons}>
              <View style={styles.Icon}>
                <Icon name="credit-card" size={20} color="#d8d8d8" style={{ marginRight: 12 }} />
                <Icon name="mobile-alt" size={20} color="#d8d8d8" style={{ marginRight: 12 }} />
              </View>
              <Icon />
            </View>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default ClubsRow;
{
  /* <View style={styles.RatingHolder}>
<Rating
  startingValue={2}
  type="star"
  imageSize={15}
  ratingCount={5}
  defaultRating={1}
  readonly={true}
  style={styles.Stars}
/>
</View> */
}
