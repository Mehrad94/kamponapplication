export const MOBILE_NUMBER = "mobileNumber";
export const IS_LOGIN = "isLogin";
export const CART_ITEM = "itemAdd";
export const TOKEN = "token";
export const MOST_DISCOUNT = "پرتخفیف ترین ها";
export const MOST_SOLD = "پرفروش ترین ها";
export const POPULAR = "محبوب ترین ها";
export const NEWEST = "جدید ترین ها";
//================ Errors ==================
export const AUTH_FAILED = 1012;
export const DELETE_CART_FAILED = 1027;
export const CART_ITEM_NOT_EXIST = 1032;

//================ Response ==================
export const AUTH_SUCCESS = 2022;
export const SUCCESS_EXIST_IN_CART = 2057;

//============== Checks ===========================
export const CART_EXIST = 5047;

export const DELETE_SUCESS = 3064;
//=============== CART ================
export const NO_ITEM_IN_CART = "سبد خرید خالی است";
// ======================SPINNER ================
export const SPIN = "چرخش گردونه";
export const TRY_YOUR_CHANSE = "شانس خود را امتحان کتید ";
export const YOUR_REWARD = "جایزه شما";
export const NEXT_SPIN = "چرخونه بعدی تا :";
//========================  PROFILE ==================
export const USER_INFO = "مشخصات کاربر";
export const MY_KAMPONS = "کمپن های من";
export const MY_TRANSACTIONS = "تراکنش ها";
export const TERM_OF_USE = "قوانین و مقررات";
export const COMMON_QUESTIONS = "سوالات متداول";
export const CONNECT_WITH_US = "ارتباط با ما";
export const ABOUT_US = "درباره ما";
export const YOUR_MENTIONS = "نطرات کاربران";
export const SUPPORT = "پشتیبانی";
export const EXIT_USER = "خروج";
//======================== HOME ITEMS ==================
export const MOST_VIEW = "بیشترین بازدید ";
export const MOST_NEW = "جدیدترین";
export const TODAY_OFFER = "پیشنهاد امروز";
export const MOST_DISCOUNT_HOME = "پر تخفیف ترین ها";
//=========================  FILTER ITEMS ==============
export const RASHT = "رشت";
export const ASTANE = "آستانه";
export const LAHIJAN = "لاهیجان";
export const DIS_FILTER = "تخفیف ها";
export const CLUB_FILTER = "یاشگاه ها";
export const REMOVE_FILETR = "حذف";
export const CITY = "شهر";
export const TYPE = "نوع";
export const FILTER_ACCORDING_CITY = "فیلنر بر اساس شهر";
export const FILTER_ACCORDING_TYPE = "فیلتر بر اساس نوع";
