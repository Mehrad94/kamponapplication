export const DISCOUNT_OWNER_DISCRIPTION =
  " لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی وبی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود. طراح گرافیک از این متن به عنوان عنصری از تربندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیک";

export const ABOUT_OWNER_TITLE = "درباره ";

export const COMMENTS = "نظرات کاربران";

export const ACCORDION_DATA = [
  {
    title: ABOUT_OWNER_TITLE,
    content: DISCOUNT_OWNER_DISCRIPTION,
  },
  { title: COMMENTS, content: "klnklnklnlkn" },
];
export const FLATLIST_DATA = [
  "https://picsum.photos/536/354",
  "https://picsum.photos/id/237/536/354",
  "https://picsum.photos/seed/picsum/536/354",
  "https://picsum.photos/id/1084/536/354?grayscale",
];
