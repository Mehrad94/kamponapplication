import axios from "./AxiosConfigue";
export const getCategoriesData = async (catId) => {
  return axios
    .get(`/category/${catId}`)
    .then((res) => res.data)
    .catch((e) => e);
};
