import axios from "./AxiosConfigue";
export const getSplashData = async () => {
  return axios
    .get("/splashscreen")
    .then((res) => {
      return res.data;
    })
    .catch((e) => {
      return e;
    });
};
