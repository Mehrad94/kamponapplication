import axios from "./AxiosConfigue";
export const DeleteFromCart = async (discountId) => {
  console.log({ discountId });

  return axios
    .delete(`/cart/${discountId}`)
    .then((res) => {
      return res.data;
    })
    .catch((e) => {
      return e;
    });
};
