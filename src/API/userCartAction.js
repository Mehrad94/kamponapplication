import Axios from "./AxiosConfigue";
export const userCartAction = async (discountId, count = 1) => {
  console.log({ discountId });
  console.log({ count });

  return Axios.post("/cart", { discountId, count })
    .then((res) => {
      return res.data;
    })
    .catch((e) => {
      return e;
    });
};
