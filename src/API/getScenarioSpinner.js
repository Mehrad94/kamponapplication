import axios from "./AxiosConfigue";
export const getScenarioSpinner = async () => {
  return axios
    .get("/scenario/spinInformation")
    .then((res) => {
      return res.data;
    })
    .catch((e) => {
      return e;
    });
};
