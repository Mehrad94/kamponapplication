import Axios from "./AxiosConfigue";
export const getItemsInCart = async () => {
  return Axios.get("cart")
    .then((res) => {
      return res.data;
    })
    .catch((e) => {
      return e;
    });
};
