import axios from "./AxiosConfigue";
export const getCategories = async () => {
  return axios
    .get("/category")
    .then((res) => res.data)
    .catch((e) => e);
};
