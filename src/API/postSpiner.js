import axios from "./AxiosConfigue";
export const postSpinner = async (gift) => {
  return axios
    .post("/scenario/spin", { gift })
    .then((res) => {
      return res.data;
    })
    .catch((e) => {
      return 1010;
    });
};
