import AsyncStorage from "@react-native-community/async-storage";
export const checkTokenInDevice = async () => {
  const avToken = await AsyncStorage.getItem("token");
  if (avToken) {
    return true;
  } else {
    return false;
  }
};
