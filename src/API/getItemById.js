import axios from "./AxiosConfigue";

export const getItemById = async (type, id) => {
  console.log({ type, id });

  if (type === "dis") {
    return axios
      .get(`/discount/${id}`)
      .then((res) => {
        return res.data;
      })
      .catch((e) => {
        return e;
      });
  }
  if (type === "club") {
    return axios
      .get(`/discount/${id}`)
      .then((res) => {
        return res.data;
      })
      .catch((e) => {
        return e;
      });
  }
};
