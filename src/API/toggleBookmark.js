import axios from "./AxiosConfigue";
import { CheckToken } from ".";
import { ADD_BOOKMARK, BOOKMARK_ADDED, NO_INTERNET, SMT_WRONG } from "../Values/Strings";
export const toggleBookmark = async (id, type) => {
  return axios
    .post("/bookmark", { id, type })
    .then((res) => {
      return res.data.MSG;
    })
    .catch((e) => {
      console.log({ e });

      if (e.message === "Network Error") {
        return NO_INTERNET;
      } else {
        return SMT_WRONG;
      }
    });
};
