import Icon from "react-native-vector-icons/FontAwesome5";
import React from "react";
import { I18nManager } from "react-native";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createMaterialTopTabNavigator } from "react-navigation-tabs";
import AuthScreen from "./src/Screens/AuthScreen/AuthScreen";
import HomeScreen from "./src/Screens/HomeScreen/index";
import SplashScreen from "./src/Screens/SplashScreen";
import { Provider as DataContext } from "./src/Context/DataContext";
import { Provider as CartScreenStepsContext } from "./src/Context/CartScreenStepsContext";
import { Provider as AuthContext } from "./src/Context/AuthContext";
import { Provider as UserDataProvider } from "./src/Context/userDataContext";
import ClubsScreen from "./src/Screens/ClubsScreen";
import FlippScreen from "./src/Screens/FlippScreen";
import ProfileScreen from "./src/Screens/ProfileScreen";
import CartScreen from "./src/Screens/CartScreen";
import { setNavigator } from "./navigationRef";
import PushPole from "pushpole-react-native";
import DiscountsScreen from "./src/Screens/DiscountsScreen";
import CategoryScreen from "./src/Screens/CategoryScreen";
import { MAIN_COLOR } from "./src/Values/Colors";
import FortuneWheel from "./src/Screens/FortuneWheel";
import KamponScreen from "./src/Screens/ProfileScreen/KamponScreen";
import TransActionScreen from "./src/Screens/ProfileScreen/TransactonScreen";
import { Root } from "native-base";
import Bookmarks from "./src/Screens/ProfileScreen/Bookmarks";
import AboutUs from './src/Screens/ProfileScreen/AboutUs';
import ContactUs from './src/Screens/ProfileScreen/ContactUs'
I18nManager.allowRTL(false);
// import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";

PushPole.initialize(true);
const HomeStack = createStackNavigator(
  {
    Home: { screen: HomeScreen, path: "Home" },
    CategoriesScreen: CategoryScreen,
    ClubsScreen,
    DiscountsScreen,
  },
  { defaultNavigationOptions: { header: null } }
);
const CategorySccreen = createStackNavigator(
  {
    FlippScreen,
    ClubsScreen,
    DiscountsScreen,
  },
  { defaultNavigationOptions: { header: null } }
);
const ProfileStack = createStackNavigator(
  {
    ProfileScreen,
    KamponScreen,
    TransActionScreen,
    Bookmarks,
    ClubsScreen,
    AboutUs,
    ContactUs
  },
  { defaultNavigationOptions: { header: null } }
);
const Tabs = createMaterialTopTabNavigator(
  {
    Profile: {
      screen: ProfileStack,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Icon name="user" color={tintColor} size={25} />,
      },
    },
    WheelScreen: {
      screen: FortuneWheel,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Icon name="plus" color={tintColor} size={25} />,
      },
    },

    CartScreen: {
      screen: CartScreen,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Icon name="shopping-bag" color={tintColor} size={25} />,
      },
      path: "Cart",
    },
    SearchScreen: {
      screen: CategorySccreen,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Icon name="store" color={tintColor} size={25} />,
      },
    },

    HomeScreen: {
      screen: HomeStack,

      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Icon name="home" color={tintColor} size={25} />,
      },
    },
  },

  {
    navigationOptions: { swipeEnabled: true },
    lazy: true,
    tabBarPosition: "bottom",
    initialRouteName: "HomeScreen",
    backBehavior: "initialRoute",
    tabBarOptions: {
      contentContainerStyle: { alignItems: "center", justifyContent: "center" },
      style: { backgroundColor: "white" },
      showIcon: true,
      showLabel: false,
      activeBackgroundColor: "#fff",
      inactiveBackgroundColor: "#fff",
      activeTintColor: "#26c6da",
      inactiveTintColor: "#aaa",
      iconStyle: { alignSelf: "center", width: 30 },
      pressColor: MAIN_COLOR,
      allowFontScaling: true,
      indicatorStyle: { backgroundColor: MAIN_COLOR },
    },
  }
);

const switchNavigator = createSwitchNavigator({
  Splash: SplashScreen,
  AuthFlow: AuthScreen,
  LoginFlow: AuthScreen,
  Tabs,
});
const prefix = "https://www.kampon.ir";

const App = createAppContainer(switchNavigator);
export default () => {
  return (
    <AuthContext>
      <DataContext>
        <CartScreenStepsContext>
          <Root>
            <UserDataProvider>
              <App ref={(navigator) => setNavigator(navigator)} uriPrefix={prefix} />
            </UserDataProvider>
          </Root>
        </CartScreenStepsContext>
      </DataContext>
    </AuthContext>
  );
};
