import { StyleSheet } from "react-native";
import Constants from "../../../Values/Constant";

export default (styles = StyleSheet.create({
  LeftContent: {
    height: "100%",
    width: "60%",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "flex-start",
    marginTop: 16
  },
  disDescrip: {
    textAlign: "center",
    fontSize: 18,
    marginBottom: 10
  },
  RightContent: {
    height: "100%",
    width: "40%",
    justifyContent: "flex-start",
    marginTop: 16
  },
  ContentContainer: {
    flexDirection: "column",
    alignItems: "center"
  },

  ContentLastPrice: {
    marginRight: 8,
    textDecorationLine: "line-through",

    color: "red",
    borderBottomWidth: 1
  },
  ContentPrice: {
    fontSize: 18,
    //lineHeight: 50,
    color: "green"
  },
  ContentPercent: {
    backgroundColor: "rgba(255,0, 50 ,0.7)",
    borderRadius: 100,
    marginLeft: 30,
    padding: 4
  },
  sellCount: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  discountPercentBg: {
    position: "absolute",
    marginTop: 10,
    width: 48,
    height: 48,
    backgroundColor: "red",
    right: 0
  },
  discountPercent: {
    fontFamily: "iran_sans",
    color: "white",
    fontSize: 15
  }
}));
