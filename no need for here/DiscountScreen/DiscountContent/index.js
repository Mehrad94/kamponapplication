import React, { Component } from "react";
import { Text, View, ImageBackground } from "react-native";
import {
  iranSans,
  h3,
  h2,
  h4,
  s48,
  h5,
  centerAll,
  fWhite,
  posAbs,
  bgMainColor,
  center
} from "../../../Values/Theme";
import styles from "./styles";
const irAmount = require("iramount");
class DiscountContent extends Component {
  render() {
    const Data = this.props.data;
    const newPrice = new irAmount(Data.disNewPrice).digitGrouped();
    const realPrice = new irAmount(Data.disRealPrice).digitGrouped();

    return (
      <View style={[styles.ContentContainer, centerAll]}>
        <Text numberOfLines={3} style={[styles.disDescrip, h4]}>
          {Data.disDescription}
        </Text>
        <View style={styles.ContentContainer}>
          <Text style={[h3, iranSans, { color: "green" }]}>{newPrice} تومان</Text>
          <Text
            style={[h4, iranSans, { textDecorationLine: "line-through", color: "red" }]}
          >
            {realPrice}
          </Text>
        </View>
        <ImageBackground
          style={[s48, centerAll, posAbs, { right: 0, backgroundColor: "green" }]}
          //source={require("../../../Assets/Png/discount.png")}
        >
          <Text style={[iranSans, h5, fWhite]}>%{Data.disPercent}</Text>
        </ImageBackground>
      </View>
    );
  }
}

export default DiscountContent;
