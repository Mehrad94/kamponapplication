import { StyleSheet, Dimensions } from "react-native";
import Constants from "../../Values/Constant";

export default (styles = StyleSheet.create({
  Container: {
    flex: 1,
    padding: 5,
    flexDirection: "column"
  },

  TitleDiscountScreen: {
    margin: 10,
    alignSelf: "center"
  },

  TabContent: {
    flex: 1,
    height: Constants.Dimension.ScreenWidth(1),
    width: Dimensions.get("screen").width
  },

  Footer: {
    position: "absolute",
    width: "100%",
    height: Constants.Dimension.ScreenWidth(0.1),
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: "#f7f5f5",
    flexDirection: "row",
    borderTopLeftRadius: 30
  },
  FooterButtom: {
    width: Constants.Dimension.ScreenWidth(0.5),

    alignItems: "center",
    justifyContent: "center"
  },
  FooterIcons: {
    //backgroundColor: "#757575",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    // borderTopLeftRadius: -30,
    width: Constants.Dimension.ScreenWidth(0.5)
  }
}));
