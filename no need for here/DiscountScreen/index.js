import axios from "axios";
import React, { useEffect, useState } from "react";
import { BackHandler, ScrollView, Text, ToastAndroid, TouchableOpacity, View } from "react-native";
import { Dialog } from "react-native-simple-dialogs";
import { CheckLogin, DeleteFromCart, PostItemToCart, toggleBookmark } from "../../API";
import { CheckDiscountInCart } from "../../API/CheckDiscountInCart";
import { Indicator } from "../../Components/Indicator";
import { CheckConnection } from "../../Utils";
import { MAIN_COLOR } from "../../Values/Colors";
import { AUTH_FAILED, BOOKMARK_ADDED, GET_DISCOUNT_INFO, INTERNET_ERROR_MSG, MSG_SMT_WRONG, NO_INTERNET } from "../../Values/Strings";
import { bgMainColor, bgRed300, centerAll, Flex, fRow, fWhite, h3, h4, h6, iranSans, m8, pad16, r8, Tac, tCenter } from "../../Values/Theme";
import DiscountSliderList from "../Lists/DiscountSliderList";
import TabScreen from "../TabScreen";
import DiscountContent from "./DiscountContent";
import DiscountShare from "./DiscountShare";
import styles from "./styles";

const DiscountScreen = ({ navigation }) => {
  const [isLoading, setLoading] = useState(true);
  const [btnLoading, setBtnLoading] = useState(false);
  const [discountDetails, setDiscountDetails] = useState([]);
  const [visible, setVisible] = useState(false);
  const [isAddInCart, setDiscountInCart] = useState(false);
  const [itemId, setItemId] = useState("");
  const [showAlert, setShowAlert] = useState(false);

  const disId = navigation.getParam("disId", "NO-ID");

  const handleBackPress = () => {
    navigation.pop();
    return true;
  };

  const _getDiscountInfo = async () => {
    const discountGetDetails = await axios.get(GET_DISCOUNT_INFO + "/" + disId);
    const discountDetails = discountGetDetails.data;
    setDiscountDetails(discountDetails);
    setDiscountInCart(await CheckDiscountInCart(discountDetails._id));
    setLoading(false);
  };

  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", handleBackPress);
    _getDiscountInfo();
    setItemId(disId);
  }, []);

  _HandleAddToCart = async () => {
    setBtnLoading(true);
    if (await CheckConnection()) {
      if (await CheckLogin()) {
        if (isAddInCart) {
          // =================== Remove from  cart  ===============
          if (await DeleteFromCart(discountDetails._id)) {
            setDiscountInCart(false);
            setBtnLoading(false);
          } else {
            setBtnLoading(false);
            ToastAndroid.show("لطفا دوباره تلاش کنید", ToastAndroid.SHORT);
          }
        } else {
          // =================== Add to  cart  ===============
          if (await PostItemToCart(discountDetails._id, 1)) {
            setBtnLoading(false);
            setDiscountInCart(true);
          } else {
            setBtnLoading(false);
            ToastAndroid.show("لطفا دوباره تلاش کنید", ToastAndroid.SHORT);
          }
        }
      } else {
        setBtnLoading(false);
        setShowAlert(true);
      }
    } else {
      // =================== User hasn't Internet Connection  ===============
      setBtnLoading(false);
      ToastAndroid.show(INTERNET_ERROR_MSG, ToastAndroid.SHORT);
    }
  };

  _AddReq = () => {
    setShowAlert(false);
    navigation.navigate("Login");
  };

  _addToBookmark = async () => {
    const resAddBookmark = await toggleBookmark(disId);
    switch (resAddBookmark) {
      case BOOKMARK_ADDED:
        break;
      case NO_INTERNET:
        ToastAndroid.show(INTERNET_ERROR_MSG, ToastAndroid.SHORT);
        break;
      case AUTH_FAILED:
        setShowAlert(true);
        break;
      default:
        ToastAndroid.show(MSG_SMT_WRONG, ToastAndroid.SHORT);
        break;
    }
  };

  if (isLoading) return <Indicator />;
  else {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView style={[Flex]} showsVerticalScrollIndicator={false}>
          <View style={[centerAll]}>
            <DiscountSliderList slides={discountDetails.disImages} />
          </View>
          <Text style={[iranSans, Tac, h3]}> {discountDetails.disTitle}</Text>
          <DiscountContent data={discountDetails} />
          <View style={[styles.TabContent]}>
            <TabScreen data={discountDetails} />
          </View>
        </ScrollView>
        <View style={styles.Footer}>
          <TouchableOpacity
            disabled={btnLoading}
            style={[styles.FooterButtom, isAddInCart ? { backgroundColor: "red" } : { backgroundColor: "#50C3C6" }]}
            onPress={_HandleAddToCart}
          >
            {btnLoading ? (
              <Indicator size={20} count={3} color={"white"} />
            ) : (
              <Text style={[fWhite, iranSans]}>{isAddInCart ? "حذف از سبد خرید" : "افزودن به سبد خرید"}</Text>
            )}
          </TouchableOpacity>
          <View>
            <DiscountShare discountId={itemId} />
            <Icon name={"ic_bookmark"} size={30} color={MAIN_COLOR} />
          </View>
        </View>

        <Dialog
          titleStyle={[iranSans, h4, tCenter]}
          dialogStyle={[r8]}
          visible={showAlert}
          title="بریم برای خرید"
          onTouchOutside={() => setShowAlert(false)}
        >
          <View>
            <Text style={[iranSans, tCenter]}>کاربر گرامی برای شروع مراحل خرید لطفا ثبت نام نمایید</Text>
            <View style={[fRow, centerAll]}>
              <TouchableOpacity onPress={this._AddReq} style={[{ width: 100, height: 32 }, centerAll, pad16, m8, bgMainColor, r8]}>
                <Text style={[fWhite, iranSans, r8, h6]}>ثبت</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  setShowAlert(false);
                }}
                style={[{ width: 100, height: 32 }, centerAll, pad16, m8, bgRed300, r8]}
              >
                <Text style={[fWhite, iranSans, r8, h6]}>انصراف</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Dialog>
      </View>
    );
  }
};

export default DiscountScreen;
