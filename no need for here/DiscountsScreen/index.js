import axios from "axios";
import React, { useEffect, useState } from "react";
import {
  BackHandler,
  ImageBackground,
  ScrollView,
  Text,
  View,
  Dimensions,
  Image
} from "react-native";
import { CheckDiscountInCart } from "../../API/CheckDiscountInCart";
import { Indicator } from "../../components/Indicator";
import {
  GET_DISCOUNT_INFO,
  NEW_PRICE,
  TOMAN,
  OLD_PRICE,
  PHONES,
  ADDRESS
} from "../../Values/Strings";
import {
  bgAccentColor,
  bgWhite,
  centerAll,
  el1,
  fBlack,
  fGreen,
  Flex,
  fLineT,
  fRed,
  fRow,
  fWhite,
  h2,
  h3,
  h4,
  h5,
  iranSans,
  m8,
  mH16,
  mV8,
  pad8,
  padH8,
  padV8,
  posAbs,
  r8,
  s48,
  spaceB,
  bgGray,
  s32,
  center,
  s24,
  mV4
} from "../../Values/Theme";
import KamponFooter from "./KamponFooter";
import KamponTab from "./KamponTab";
import KamponSliderList from "../DiscountsScreen/KamponSliderList";
import { TouchableOpacity } from "react-native-gesture-handler";
const irAmount = require("iramount");

const DiscountsScreen = ({ navigation }) => {
  const [discount, setDiscount] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const [isAddInCart, setDiscountInCart] = useState(false);
  console.log({ StateDiscountScreen: discount });

  const handleBackPress = () => {
    navigation.pop();
    return true;
  };
  let newPrice = "";
  let realPrice = "";
  const disId = navigation.getParam("disId", "NO-ID");

  const _getKamponInformation = async () => {
    try {
      const discountGetDetails = await axios.get(
        GET_DISCOUNT_INFO + "/" + disId
      );
      console.log({ DIscountDetails: discountGetDetails.data });

      const discountDetails = discountGetDetails.data;
      setDiscount(discountDetails);
      setDiscountInCart(await CheckDiscountInCart(disId));
      setLoading(false);
    } catch (error) {
      console.log({ error });
    }
  };

  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", handleBackPress);
    _getKamponInformation();
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackPress);
    };
  }, [navigation]);

  if (isLoading) return <Indicator />;
  if (!isLoading) {
    newPrice = new irAmount(discount.disNewPrice).digitGrouped();
    realPrice = new irAmount(discount.disRealPrice).digitGrouped();
  }
  const onNavigateToReport = () => {
    navigation.navigate("Report", { disId: disId });
  };
  return (
    <View style={[bgAccentColor, Flex]}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{ marginBottom: 45 }}
      >
        <View style={[centerAll]}>
          <KamponSliderList slides={discount.disImages} />
        </View>
        <View style={[bgWhite, el1, m8, pad8, r8]}>
          <Text style={[iranSans, h2, mH16]}>{discount.disTitle}</Text>
          <Text
            style={[
              iranSans,
              h5,
              { color: "rgba(0, 0, 0, 0.5)", marginHorizontal: 24 }
            ]}
          >
            {discount.ownerId.oName}
          </Text>
          <Text style={[iranSans, h4, mV8, mH16]}>
            {" "}
            {discount.disDescription}
          </Text>
          <ImageBackground
            style={[s48, centerAll, posAbs, { right: 0 }]}
            source={require("../../Assets/Png/discount.png")}
          >
            <Text style={[iranSans, h3, fWhite]}>%{discount.disPercent}</Text>
          </ImageBackground>
        </View>
        <View style={[bgWhite, el1, m8, padV8, padH8, r8]}>
          <View style={[fRow, spaceB]}>
            <Text style={[iranSans, h4, mV8, fGreen]}>{NEW_PRICE}</Text>
            <Text style={[iranSans, h4, mV8, fGreen]}>
              {newPrice} {TOMAN}
            </Text>
          </View>
          <View
            style={{ height: 1, width: "100%", backgroundColor: "#e5e5e5" }}
          />
          <View style={[fRow, spaceB]}>
            <Text style={[iranSans, h5, mV8, fRed]}>{[OLD_PRICE]}</Text>
            <Text style={[iranSans, h5, mV8, fRed, fLineT]}>
              {realPrice} {TOMAN}
            </Text>
          </View>
          <View
            style={{ height: 1, width: "100%", backgroundColor: "#e5e5e5" }}
          />
          <View style={[fRow, spaceB]}>
            <Text style={[iranSans, h5, mV8, fBlack]}>{PHONES}</Text>
            <Text style={[iranSans, h5, mV8, fBlack]}>
              {discount.ownerId.oPhoneNumber}
            </Text>
          </View>
          <View
            style={{ height: 1, width: "100%", backgroundColor: "#e5e5e5" }}
          />
          <View style={[fRow]}>
            <Text style={[iranSans, h5, mV8, fBlack]}> {ADDRESS} </Text>
            <Text
              style={[
                iranSans,
                h5,
                mV8,
                fBlack,
                { width: Dimensions.get("screen").width / 1.3 }
              ]}
            >
              {discount.ownerId.oAddress}
            </Text>
          </View>
        </View>
        <TouchableOpacity
          style={[fRow, bgWhite, el1, m8, pad8, r8, center, spaceB]}
          onPress={onNavigateToReport}
        >
          <Text style={[iranSans, h5, mV8]}>گزارش اشکال</Text>
          <Image style={[s24]} source={require("../../Assets/Png/back.png")} />
        </TouchableOpacity>
        <KamponTab tabContent={discount} />
      </ScrollView>
      <KamponFooter id={discount._id} navigate={navigation.navigate} />
    </View>
  );
};

export default DiscountsScreen;
